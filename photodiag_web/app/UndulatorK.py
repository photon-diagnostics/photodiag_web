import epics as ep
import numpy as np
import time

class UndulatorK():
    def __init__(self, maximum_energy_change_keV=1.0, name=None):
        self.maximum_energy_change_keV = maximum_energy_change_keV
    
        self.ksets = []
    #     self.gaps = []
        self.und_list = [3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

    def get_FEL_energy(self):
        val = ep.caget('SARUN:FELPHOTENE')
        return val

    def calc_new_Ksets(self, energy_target, energy_start=None):
        if not energy_start:
            energy_start = self.get_FEL_energy()
        K_start = [ep.caget(f"SARUN{tks:02d}-UIND030:K_SET") for tks in self.und_list]
        return [
            (energy_start / energy_target * (tK_start**2 + 2) - 2) ** 0.5
            for tK_start in K_start
        ]

    def get_current_k(self):
        K_start = [ep.caget(f"SARUN{tks:02d}-UIND030:K_SET") for tks in self.und_list]
        return K_start

    def change_energy(self, energy, dummy = False):
        if np.abs(energy - self.get_FEL_energy()) > self.maximum_energy_change_keV:
            raise Exception("Likely too large undulator energy change requested!!!")

        vals = self.calc_new_Ksets(energy)
        start_time = time.time()
        for kset, val in zip(self.und_list, vals):
            if dummy != True:
                ep.caput(f"SARUN{kset:02d}-UIND030:K_SET", val)
            print('Move: '+f"SARUN{kset:02d}-UIND030:K_SET"+str(val))
            

                
        
    #     sleep(0.2)
    #     for gap in self.gaps:
    #         while gap.get_change_done() == 0:
    #             sleep(0.02)
    #             if (time.time()-start_time) > 10:
    #                 print('NB: did not see all Undulators start move and stop for 10s, calling move done anyways.')
    #                 break
    #     sleep(1)

