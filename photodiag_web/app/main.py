import logging
from io import StringIO

from bokeh.io import curdoc
from bokeh.layouts import column
from bokeh.models import CustomJS, Div, TabPanel, Tabs, TextAreaInput

from photodiag_web.app import (
    panel_spectrometer_setup,
    panel_calibration,
    panel_correlation,
    panel_diode_check,
    panel_jitter,
    panel_arrival_time_tool_athos,
    panel_spect_arrival_time_tool,
    panel_spect_autocorr,
    panel_spect_int_corr,
    panel_spect_peaks,
    panel_spect_peak_count,
    panel_spect_symmetry,
    panel_spacial_sigma_evolution,
    panel_spect_fourier_transform,
    panel_spect_fourier_angle,
    panel_startup,
    panel_summary
)

doc = curdoc()
doc.title = "photodiag-web"
doc.pvs = []

stream = StringIO()
handler = logging.StreamHandler(stream)
handler.setFormatter(
    logging.Formatter(fmt="%(asctime)s %(levelname)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S")
)
logger = logging.getLogger(str(id(doc)))
logger.setLevel(logging.INFO)
logger.addHandler(handler)
# Add logger before creating panels!
doc.logger = logger

log_textareainput = TextAreaInput(title="logging output:", height=150, width=1500)

position_img = Div(text="""<img src="/app/static/aramis.png" width="1000" height="200">""")
position_tabs = Tabs(
    tabs=[
        panel_calibration.create(),
        panel_correlation.create(),
        panel_jitter.create(),
        panel_diode_check.create(),
    ]
)
position_panel = TabPanel(child=column(position_img, position_tabs), title="Position")

spectral_img = Div(text="""<img src="/app/static/sf_spectral.png" width="1000" height="200">""")
spectral_tabs = Tabs(
    tabs=[
        panel_spect_int_corr.create(),
        panel_spect_peaks.create(
            "Aramis Spectral peaks analysis", ["SARFE10-PSSS059", "SARFE10-PSSS059-LB"]
        ),
        panel_spect_peaks.create(
            "Athos Spectral peaks analysis", ["SATOP21-PMOS127-2D", "SATOP31-PMOS132-2D"]
        ),
        panel_spect_autocorr.create("Aramis/Athos Spectral autocorrelation"),
        panel_spect_symmetry.create("Symmetry"),
        panel_spacial_sigma_evolution.create("Center Evolution"),
        panel_spect_fourier_transform.create("Fourier Frequency Cut"),
        panel_spect_fourier_angle.create("Fourier Angle Analysis "),
        #panel_spect_peak_count.create("Real time spike count prediction", ["SATOP21-PMOS127-2D", "SATOP11-PSAS079","SATOP21-PMOS127-2D","SATOP31-PMOS132-2D", "SARFE10-PSSS059","SARFE10-PSSS059-LB"])
    ]
)
spectral_panel = TabPanel(child=column(spectral_img, spectral_tabs), title="Spectral")

temporal_tabs = Tabs(
    tabs=[
        panel_spect_arrival_time_tool.create(),
        panel_arrival_time_tool_athos.create(),
    ]
)
temporal_panel = TabPanel(child=column(temporal_tabs), title="Arrival time")

info_panel = TabPanel(child=column(Div()), title="Info")

startup_tabs = Tabs(
    tabs=[
        panel_spectrometer_setup.create(),
        panel_startup.create(),
    ]
)
startup_panel = TabPanel(child=column(startup_tabs), title="Startup")

summary_tabs = Tabs(
    tabs=[
        panel_summary.create(),
    ]
)
summary_panel = TabPanel(child=column(summary_tabs), title="Summary")

# Final layout
main_tabs = Tabs(
    tabs=[position_panel, spectral_panel, temporal_panel, startup_panel, summary_panel, info_panel],
    styles={"font-weight": "bold", "font-size": "20px"},
)
main_tabs.tags = [main_tabs.active]

# open wiki and reactivate previously active tab when Info panel is clicked
main_tabs_js_code = """
if (cb_obj.tabs[cb_obj.active].title == "Info") {
    cb_obj.active = cb_obj.tags[0];
    window.open('https://git.psi.ch/photon-diagnostics/swissfel-photon-diagnostics-guide/-/wikis/home');
} else {
    cb_obj.tags[0] = cb_obj.active;
}
"""
main_tabs.js_on_change("active", CustomJS(code=main_tabs_js_code))

# Helper function to recursively log TabPanel information
def print_tabs_info(tabs, level=0):
    indent = "  " * level
    for tab in tabs:
        logger.info(f"{indent}Tab title: {tab.title}, id: {tab.id}, tags: {tab.tags}")
        # If the tab's child is a Tabs layout, recurse into it
        if hasattr(tab.child, "tabs"):
            print_tabs_info(tab.child.tabs, level + 1)
        # Otherwise, if the child is a layout with children, check each child
        elif hasattr(tab.child, "children"):
            for child in tab.child.children:
                if hasattr(child, "tabs"):
                    print_tabs_info(child.tabs, level + 1)

# Log the information for all top-level tabs
print_tabs_info(main_tabs.tabs)

doc.add_root(column(main_tabs, log_textareainput))


def update_stdout():
    log_textareainput.value = stream.getvalue()


doc.add_periodic_callback(update_stdout, 1000)
