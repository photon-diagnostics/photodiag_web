from collections import deque
from datetime import datetime
from threading import Lock, Thread
import time

import bsread
import numpy as np
import epics
from bokeh.layouts import column, row
from bokeh.models import Band, ColumnDataSource, Spacer, Span, Spinner, TabPanel, Toggle, Div, Button
from bokeh.plotting import curdoc, figure
from cam_server_client import PipelineClient

# Initialize PipelineClient
pipeline_client = PipelineClient()
config_name = "SATOP31-ATT01_proc"

def create():
    doc = curdoc()
    log = doc.logger

    # ------------------------------
    # Guide Text (with a humorous tone)
    # ------------------------------
    guide_text = Div(text="""
    <div style='border: 2px solid #91d5ff; background-color: #e6f7ff; padding: 15px; border-radius: 8px;'>
      <h2 style='color:#005bb5;'>Welcome to the X-ray Arrival Time & Feedback Control Panel</h2>
      <p style='font-size: 16px;'>
        Here, you can view real-time arrival time data and control the feedback loop that adjusts our delay stage.
        <br><br>
        <b>WARNING:</b> Pressing the <i>Turn feedback on</i> button is like opening Pandora's box – once activated, the feedback loop may start moving the motor faster than you can say "where's my edge gone"!
        <br><br>
        Also, if you dare to change the numerical feedback values below, make sure you know what you're doing – your experiments (and your sanity) might depend on it!
      </p>
    </div>
    """, styles={'margin-bottom': '15px'})

    # ------------------------------
    # Read the initial configuration from the config file.
    # ------------------------------
    current_config = pipeline_client.get_config(config_name)
    # Extract feedback parameters from the nested "feedback" dictionary.
    feedback_config = current_config.get("feedback", {})
    feedback_params = {
        "global_stage": feedback_config.get("global_stage", "SLAAT31-LMOT-M816:MOT"),
        "feedback_output": feedback_config.get("feedback_output", "SLAAT01-LSCP-DRS1FNS:CH2:BSTART"),
        "feedback_buffer": feedback_config.get("feedback_buffer", 5000),
        "feedback_deadband": feedback_config.get("feedback_deadband", 250),
        "feedback_target": feedback_config.get("feedback_target", 1750),
        "feedback_step": feedback_config.get("feedback_step", 0.1)
    }

    device_channels = (
        "SATOP31-ATT01:signal",
        "SATOP31-ATT01:raw_wf",
        "SATOP31-ATT01:avg_dark_wf",
        "SATOP31-ATT01:xcorr",
        "SATOP31-ATT01:xcorr_ampl",
        "SATOP31-ATT01:edge_pos",
    )

    # Create EPICS PV and motor using values from the config.
    feedback_output_PV = epics.PV(feedback_params["feedback_output"])
    feedback_motor = epics.Motor(feedback_params["global_stage"])
    lock = Lock()

    # Create a Div widget to display the current feedback config in a blue-shaded box.
    config_div = Div(text="", styles={
        'background-color': '#e6f7ff',
        'padding': '10px',
        'border': '1px solid #91d5ff',
        'margin-bottom': '15px'
    })

    # ------------------------------
    # Figures and Data Sources (unchanged)
    # ------------------------------
    # Signal figure
    signal_fig = figure(
        height=250,
        width=1000,
        x_axis_label="Camera pixel",
        y_axis_label="Spectral intensity [arb]",
        tools="pan,wheel_zoom,save,reset",
    )
    signal_fig.toolbar.logo = None
    signal_fig_line_source = ColumnDataSource(dict(x=[], y=[]))
    signal_fig.line(source=signal_fig_line_source)

    # Waveform figure
    waveform_fig = figure(
        height=250,
        width=1000,
        x_axis_label="Camera pixel",
        y_axis_label="Spectral intensity [arb]",
        tools="pan,wheel_zoom,save,reset",
    )
    waveform_fig.toolbar.logo = None
    waveform_line1_source = ColumnDataSource(dict(x=[], y=[]))
    waveform_line2_source = ColumnDataSource(dict(x=[], y=[]))
    waveform_fig.line(source=waveform_line1_source, legend_label="Raw waveform")
    waveform_fig.line(source=waveform_line2_source, line_color="black", legend_label="Avg dark waveform")

    # Cross correlation figure
    cross_corr_fig = figure(
        height=250,
        width=500,
        x_axis_label="Camera pixel",
        y_axis_label="Spectral cross corr intensity [arb]",
        tools="pan,wheel_zoom,save,reset",
    )
    cross_corr_fig.toolbar.logo = None
    cross_corr_line_source = ColumnDataSource(dict(x=[], y=[]))
    cross_corr_fig.line(source=cross_corr_line_source)

    # Add edge position span to all figures
    edge_pos_span = Span(location=None, dimension="height", line_color="red", line_dash="dashed")
    signal_fig.add_layout(edge_pos_span)
    waveform_fig.add_layout(edge_pos_span)
    cross_corr_fig.add_layout(edge_pos_span)

    # Edge position figure
    edge_pos_fig = figure(
        height=250,
        width=500,
        x_axis_label="Camera pixel",
        y_axis_label="Edge counts",
        tools="pan,wheel_zoom,save,reset",
    )
    edge_pos_fig.toolbar.logo = None
    edge_pos_quad_source = ColumnDataSource(dict(left=[], right=[], top=[]))
    edge_pos_fig.quad(source=edge_pos_quad_source, bottom=0)

    # Average edge position figure
    edge_pos_avg_fig = figure(
        height=250,
        width=500,
        x_axis_label="Time",
        x_axis_type="datetime",
        y_axis_label="Avg edge position [pix]",
        tools="pan,wheel_zoom,save,reset",
    )
    edge_pos_avg_fig.toolbar.logo = None
    edge_pos_avg_fig_line_source = ColumnDataSource(dict(x=[], y=[], lower=[], upper=[]))
    edge_pos_avg_fig.line(source=edge_pos_avg_fig_line_source)
    edge_pos_avg_fig.add_layout(Band(base="x", source=edge_pos_avg_fig_line_source))

    # Global delay figure
    global_delay_fig = figure(
        height=250,
        width=500,
        x_axis_label="Time",
        x_axis_type="datetime",
        y_axis_label="Global delay [mm]",
        tools="pan,wheel_zoom,save,reset",
    )
    global_delay_fig.toolbar.logo = None
    global_delay_fig_line_source = ColumnDataSource(dict(x=[], y=[]))
    global_delay_fig.line(source=global_delay_fig_line_source)
    global_delay_fig.add_layout(Band(base="x", source=global_delay_fig_line_source))

    # Average xcorr amplitude figure
    xcorr_ampl_avg_fig = figure(
        height=250,
        width=500,
        x_axis_label="Time",
        x_axis_type="datetime",
        y_axis_label="Avg crosscorr ampl [arb]",
        tools="pan,wheel_zoom,save,reset",
    )
    xcorr_ampl_avg_fig.toolbar.logo = None
    xcorr_ampl_avg_fig_line_source = ColumnDataSource(dict(x=[], y=[], lower=[], upper=[]))
    xcorr_ampl_avg_fig.line(source=xcorr_ampl_avg_fig_line_source)
    xcorr_ampl_avg_fig.add_layout(Band(base="x", source=xcorr_ampl_avg_fig_line_source))

    # FFT of edge position figure
    edge_pos_fft_fig = figure(
        height=250,
        width=500,
        x_axis_label="Frequency [Hz]",
        y_axis_label="FFT [arb]",
        tools="pan,wheel_zoom,save,reset",
    )
    edge_pos_fft_fig.toolbar.logo = None
    edge_pos_fft_fig_line_source = ColumnDataSource(dict(x=[], y=[]))
    edge_pos_fft_fig.line(source=edge_pos_fft_fig_line_source)

    # ------------------------------
    # Data Buffers
    # ------------------------------
    data_cache = None
    buffer_edge_pos = deque()
    buffer_xcorr_ampl = deque()
    buffer_edge_pos_fft = deque(maxlen=1000)
    buffer_edge_pos_fft_pid = deque(maxlen=1000)
    buffer_feedback = deque(maxlen=feedback_params["feedback_buffer"])

    # ------------------------------
    # Data Collection Thread
    # ------------------------------
    def _collect_data():
        nonlocal data_cache, buffer_edge_pos, buffer_xcorr_ampl
        data_cache = None
        buffer_edge_pos.clear()
        buffer_xcorr_ampl.clear()
        try:
            with bsread.source(host="sf-daqsync-03.psi.ch", port=9021, mode=bsread.SUB) as stream:
                while update_toggle.active:
                    msg_data = stream.receive().data
                    values = []
                    for ch in device_channels:
                        values.append(msg_data.data.get(ch).value if msg_data.data.get(ch) is not None else None)
                    with lock:
                        data_cache = values
                        if values[-1] is not None and values[-2] is not None:
                            buffer_edge_pos.append(values[-1])
                            buffer_xcorr_ampl.append(values[-2])
                            buffer_edge_pos_fft.append(values[-1])
                        else:
                            buffer_edge_pos_fft.append(np.nan)
                        buffer_edge_pos_fft_pid.append(msg_data.pulse_id)
        except Exception as e:
            log.error(e)

    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=100, step=100, low=100)

    update_plots_periodic_callback = None

    def update_toggle_callback(_attr, _old, new):
        nonlocal update_plots_periodic_callback
        if new:
            thread = Thread(target=_collect_data)
            thread.start()
            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 500)
            num_shots_spinner.disabled = True
            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:
            doc.remove_periodic_callback(update_plots_periodic_callback)
            num_shots_spinner.disabled = False
            update_toggle.label = "Update"
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)

    def feedback_toggle_callback(_attr, _old, new):
        if new:
            feedback_toggle.label = "Turn feedback off"
            feedback_toggle.button_type = "success"
        else:
            feedback_toggle.label = "Turn feedback on"
            feedback_toggle.button_type = "primary"

    feedback_toggle = Toggle(label="Turn feedback on", button_type="primary")
    feedback_toggle.on_change("active", feedback_toggle_callback)

    # ------------------------------
    # Dynamic Config Update (Polling)
    # ------------------------------
    def update_config():
        """
        Poll the configuration file using the PipelineClient and update
        the local feedback_params dictionary as well as the config display.
        """
        new_config = pipeline_client.get_config(config_name)
        new_feedback = new_config.get("feedback", {})
        feedback_params["global_stage"] = new_feedback.get("global_stage", feedback_params["global_stage"])
        feedback_params["feedback_output"] = new_feedback.get("feedback_output", feedback_params["feedback_output"])
        feedback_params["feedback_buffer"] = new_feedback.get("feedback_buffer", feedback_params["feedback_buffer"])
        feedback_params["feedback_deadband"] = new_feedback.get("feedback_deadband", feedback_params["feedback_deadband"])
        feedback_params["feedback_target"] = new_feedback.get("feedback_target", feedback_params["feedback_target"])
        feedback_params["feedback_step"] = new_feedback.get("feedback_step", feedback_params["feedback_step"])
        # Update config display Div
        config_div.text = (
            "<b>Current Feedback Config:</b><br>"
            "Global Stage: {}<br>"
            "Feedback Output: {}<br>"
            "Feedback Buffer: {}<br>"
            "Feedback Deadband: {}<br>"
            "Feedback Target: {}<br>"
            "Feedback Step: {}"
        ).format(
            feedback_params["global_stage"],
            feedback_params["feedback_output"],
            feedback_params["feedback_buffer"],
            feedback_params["feedback_deadband"],
            feedback_params["feedback_target"],
            feedback_params["feedback_step"]
        )

    # ------------------------------
    # Manual Feedback Parameter Editing Section
    # ------------------------------
    manual_edit_toggle = Toggle(label="Enable manual feedback config editing", button_type="primary", active=False)
    
    spinner_feedback_buffer = Spinner(title="Feedback Buffer", mode="int", value=feedback_params["feedback_buffer"], step=100, low=100)
    spinner_feedback_deadband = Spinner(title="Feedback Deadband", mode="int", value=feedback_params["feedback_deadband"], step=10, low=0)
    spinner_feedback_target = Spinner(title="Feedback Target", mode="int", value=feedback_params["feedback_target"], step=50, low=0)
    spinner_feedback_step = Spinner(title="Feedback Step", mode="float", value=feedback_params["feedback_step"], step=0.01, low=0)

    confirm_button = Button(label="Confirm Feedback Config Update", button_type="warning", disabled=True)

    def manual_edit_toggle_callback(attr, old, new):
        confirm_button.disabled = not new
        spinner_feedback_buffer.disabled = not new
        spinner_feedback_deadband.disabled = not new
        spinner_feedback_target.disabled = not new
        spinner_feedback_step.disabled = not new

    manual_edit_toggle.on_change("active", manual_edit_toggle_callback)

    def confirm_feedback_update():
        feedback_params["feedback_buffer"] = spinner_feedback_buffer.value
        feedback_params["feedback_deadband"] = spinner_feedback_deadband.value
        feedback_params["feedback_target"] = spinner_feedback_target.value
        feedback_params["feedback_step"] = spinner_feedback_step.value
        new_feedback = {
            "global_stage": feedback_params["global_stage"],
            "feedback_output": feedback_params["feedback_output"],
            "feedback_buffer": feedback_params["feedback_buffer"],
            "feedback_deadband": feedback_params["feedback_deadband"],
            "feedback_target": feedback_params["feedback_target"],
            "feedback_step": feedback_params["feedback_step"]
        }
        pipeline_client.set_instance_config(config_name, {"feedback": new_feedback})
        log.info("Feedback configuration updated via manual editing.")
        update_config()
    confirm_button.on_click(confirm_feedback_update)

    # ------------------------------
    # Plot Update Callback (asynchronous)
    # ------------------------------
    async def _update_plots():
        update_config()

        if len(buffer_edge_pos) == 0:
            signal_fig_line_source.data.update(x=[], y=[])
            waveform_line1_source.data.update(x=[], y=[])
            waveform_line2_source.data.update(x=[], y=[])
            cross_corr_line_source.data.update(x=[], y=[])
            edge_pos_quad_source.data.update(left=[], right=[], top=[])
            edge_pos_avg_fig_line_source.data.update(x=[], y=[])
            xcorr_ampl_avg_fig_line_source.data.update(x=[], y=[])
            global_delay_fig_line_source.data.update(x=[], y=[])
            return

        with lock:
            signal, raw_wf, avg_dark_wf, xcorr, _, last_edge_pos = data_cache
            buffer_edge_pos_cp = list(buffer_edge_pos)
            buffer_xcorr_ampl_cp = list(buffer_xcorr_ampl)
            buffer_edge_pos_fft_cp = list(buffer_edge_pos_fft)
            buffer_edge_pos_fft_pid_cp = list(buffer_edge_pos_fft_pid)

        edge_pos = np.array(buffer_edge_pos_cp)
        xcorr_ampl = np.array(buffer_xcorr_ampl_cp)
        counts, edges = np.histogram(edge_pos)

        edge_pos_avg = np.mean(edge_pos)
        edge_pos_std = np.std(edge_pos)
        xcorr_ampl_avg = np.mean(xcorr_ampl)
        xcorr_ampl_std = np.std(xcorr_ampl)

        # Feedback to move delay stage position
        feedback_output_PV.put(edge_pos_avg)
        if feedback_toggle.active:
            if edge_pos_avg < feedback_params["feedback_target"] - feedback_params["feedback_deadband"]:
                feedback_motor.move(feedback_params["feedback_step"], relative=True, wait=True)
            elif edge_pos_avg > feedback_params["feedback_target"] + feedback_params["feedback_deadband"]:
                feedback_motor.move(-feedback_params["feedback_step"], relative=True, wait=True)

        edge_pos_fft = np.array(buffer_edge_pos_fft_cp)
        edge_pos_fft_pid = np.array(buffer_edge_pos_fft_pid_cp)
        nan_ind = np.isnan(edge_pos_fft)
        edge_pos_fft = edge_pos_fft[~nan_ind]
        edge_pos_fft_pid = edge_pos_fft_pid[~nan_ind]

        if len(edge_pos_fft_pid) > 0:
            edge_pos_fft_interp = np.interp(
                x=np.arange(min(edge_pos_fft_pid), max(edge_pos_fft_pid) + 1),
                xp=edge_pos_fft_pid,
                fp=edge_pos_fft,
            )
            fft_res = np.abs(np.fft.fft(edge_pos_fft_interp))
            freqs = np.fft.fftfreq(edge_pos_fft_interp.size, 1 / 100)
            fft_n = len(fft_res)
            fft_res = fft_res[1 : fft_n // 2]
            freqs = freqs[1 : fft_n // 2]
            edge_pos_fft_fig_line_source.data.update(x=freqs, y=fft_res)

        global_stage_position = feedback_motor.get_position()

        if signal is None:
            signal_fig_line_source.data.update(x=[], y=[])
        else:
            signal_fig_line_source.data.update(x=np.arange(len(signal)), y=signal)

        if xcorr is None:
            cross_corr_line_source.data.update(x=[], y=[])
        else:
            len_diff = int(np.ceil((len(signal) - len(xcorr)) / 2))
            cross_corr_line_source.data.update(x=np.arange(len(xcorr)) + len_diff, y=xcorr)

        edge_pos_quad_source.data.update(left=edges[:-1], right=edges[1:], top=counts)
        edge_pos_span.location = last_edge_pos

        # Check for None before updating waveform sources
        if raw_wf is not None:
            waveform_line1_source.data.update(x=np.arange(len(raw_wf)), y=raw_wf)
        else:
            waveform_line1_source.data.update(x=[], y=[])
            log.info(f"{datetime.now()}: raw_wf is None; updating waveform_line1_source with empty data.")

        if avg_dark_wf is not None:
            waveform_line2_source.data.update(x=np.arange(len(avg_dark_wf)), y=avg_dark_wf)
        else:
            waveform_line2_source.data.update(x=[], y=[])
            log.info(f"{datetime.now()}: avg_dark_wf is None; updating waveform_line2_source with empty data.")

        stream_t = datetime.now()
        plot_length = 3600
        edge_pos_avg_fig_line_source.stream(
            dict(
                x=[stream_t],
                y=[edge_pos_avg],
                lower=[edge_pos_avg - edge_pos_std],
                upper=[edge_pos_avg + edge_pos_std],
            ),
            rollover=plot_length,
        )
        xcorr_ampl_avg_fig_line_source.stream(
            dict(
                x=[stream_t],
                y=[xcorr_ampl_avg],
                lower=[xcorr_ampl_avg - xcorr_ampl_std],
                upper=[xcorr_ampl_avg + xcorr_ampl_std],
            ),
            rollover=plot_length,
        )
        global_delay_fig_line_source.stream(
            dict(
                x=[stream_t],
                y=[global_stage_position],
            ),
            rollover=plot_length,
        )

    # ------------------------------
    # Layout Arrangement
    # ------------------------------
    fig_layout = row(column(signal_fig, waveform_fig), column(cross_corr_fig, edge_pos_fig))
    control_row = row(num_shots_spinner, column(Spacer(height=18), update_toggle, feedback_toggle))
    
    manual_edit_layout = column(
        Div(text="<b>Feedback Parameter Editor:</b> Only make changes if you know what you're doing – misuse may lead to unexpected motor acrobatics!"),
        manual_edit_toggle,
        row(spinner_feedback_buffer, spinner_feedback_deadband),
        row(spinner_feedback_target, spinner_feedback_step),
        confirm_button,
        styles={'border': '1px dashed #ff7f50', 'padding': '10px', 'margin-top': '15px'}
    )
    tab_layout = column(
        guide_text,
        fig_layout,
        row(edge_pos_avg_fig, xcorr_ampl_avg_fig, edge_pos_fft_fig),
        row(global_delay_fig),
        control_row,
        config_div,
        manual_edit_layout
    )

    return TabPanel(child=tab_layout, title="Furka arrival time tool")
