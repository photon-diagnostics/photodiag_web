from collections import deque
from datetime import datetime
from threading import Thread
import bsread
import numpy as np
from bokeh.layouts import column, gridplot, row
from bokeh.models import Button, ColumnDataSource, Select, Spacer, Spinner, TabPanel, Toggle, Range1d
from bokeh.plotting import curdoc, figure
from scipy.fft import fft, fftfreq

from photodiag_web import DEVICES, push_elog

def create():
    doc = curdoc()
    log = doc.logger
    device_name = ""
    device_channels = ("", "", "")

    buffer = deque()

    # xy figure
    xy_fig = figure(title=" ", height=500, width=500, tools="pan,wheel_zoom,save,reset")
    
    even_scatter_source = ColumnDataSource(dict(x=[], y=[], i=[]))
    odd_scatter_source = ColumnDataSource(dict(x=[], y=[], i=[]))
    
    xy_fig.circle(x="x", y="y", source=even_scatter_source, legend_label="even")
    xy_fig.circle(
        x="x",
        y="y",
        source=odd_scatter_source,
        line_color="red",
        fill_color="red",
        legend_label="odd",
    )
    
    xy_fig.plot.legend.click_policy = "hide"

    # ix figure
    ix_fig = figure(title=" ", height=500, width=500, tools="pan,wheel_zoom,save,reset")
    
    ix_fig.circle(x="i", y="x", source=even_scatter_source, legend_label="even")
    ix_fig.circle(
        x="i",
        y="x",
        source=odd_scatter_source,
        line_color="red",
        fill_color="red",
        legend_label="odd",
    )
    
    ix_fig.plot.legend.click_policy = "hide"

    # iy figure
    iy_fig = figure(title=" ", height=500, width=500, tools="pan,wheel_zoom,save,reset")

    iy_fig.circle(x="i", y="y", source=even_scatter_source, legend_label="even")
    iy_fig.circle(
        x="i",
        y="y",
        source=odd_scatter_source,
        line_color="red",
        fill_color="red",
        legend_label="odd",
    )

    iy_fig.plot.legend.click_policy = "hide"

    # FFT plot figures
    fft_x_fig = figure(title="FFT X Position", height=350, width=500, tools="pan,wheel_zoom,save,reset")
    fft_y_fig = figure(title="FFT Y Position", height=350, width=500, tools="pan,wheel_zoom,save,reset")
    fft_i_fig = figure(title="FFT Intensity", height=350, width=500, tools="pan,wheel_zoom,save,reset")
    fft_x_source = ColumnDataSource(dict(freq=[], amplitude=[]))
    fft_y_source = ColumnDataSource(dict(freq=[], amplitude=[]))
    fft_i_source = ColumnDataSource(dict(freq=[], amplitude=[]))
    fft_x_fig.line(x='freq', y='amplitude', source=fft_x_source, color="green")
    fft_y_fig.line(x='freq', y='amplitude', source=fft_y_source, color="blue")
    fft_i_fig.line(x='freq', y='amplitude', source=fft_i_source, color="red")

    def _collect_data():
        nonlocal buffer
        buffer = deque(maxlen=num_shots_spinner.value)

        try:
            with bsread.source(channels=device_channels) as stream:
                while update_toggle.active:
                    msg_data = stream.receive().data
                    is_odd = msg_data.pulse_id % 2
                    values = [msg_data.data.get(ch).value for ch in device_channels]
                    if not any(val is None for val in values):
                        buffer.append((is_odd, *values))

        except Exception as e:
            log.error(f"Error collecting data: {e}")

    async def _update_plots():
        if not buffer:
            xy_fig.title.text = " "
            ix_fig.title.text = " "
            iy_fig.title.text = " "
            fft_x_source.data.update(freq=[], amplitude=[])
            fft_y_source.data.update(freq=[], amplitude=[])
            fft_i_source.data.update(freq=[], amplitude=[])
            
            even_scatter_source.data.update(x=[], y=[], i=[])
            odd_scatter_source.data.update(x=[], y=[], i=[])
            return

        datetime_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        title = f"{device_name}, {datetime_now}"
        xy_fig.title.text = title
        ix_fig.title.text = title
        iy_fig.title.text = title

        data_array = np.array(list(buffer))
        is_even = data_array[:, 0] == 0
        data_even = data_array[is_even, :]
        data_odd = data_array[~is_even, :]

        even_scatter_source.data.update(x=data_even[:, 1], y=data_even[:, 2], i=list(range(len(data_even))))
        odd_scatter_source.data.update(x=data_odd[:, 1], y=data_odd[:, 2], i=list(range(len(data_odd))))

        #update_fft_plots(data_even[:, 1:])  # Update FFT plots for even data
        update_fft_plots(data_array[:, 1:])   # Update FFT plots for odd data

    def update_fft_plots(data):
        N = len(data)
        T = 1.0 / 1100.0  # Sample spacing, adjust as needed

        # Extract data for FFT
        x_positions = data[:, 0]
        y_positions = data[:, 1]
        intensities = data[:, 2]
        rep_rate = 100 # add this to channel list in future

        # X position FFT
        # x_fft = fft(x_positions)
        # x_freqs = fftfreq(N, T)[:N//2]
        # fft_x_source.data.update(freq=x_freqs, amplitude=2.0/N * np.abs(x_fft[:N//2]))
        x=np.arange(0,np.ceil(100),np.ceil(100)/len(y_positions))
        power = np.abs(np.fft.fft(x_positions))**2
        axis_len = int(len(x)*0.55)
        fft_x_source.data.update(freq=x[1:axis_len], amplitude=power[1:axis_len])

        # Y position FFT
        # y_fft = fft(y_positions)
        # y_freqs = fftfreq(N, T)[:N//2]
        x=np.arange(0,np.ceil(100),np.ceil(100)/len(y_positions))
        power = np.abs(np.fft.fft(y_positions))**2
        axis_len = int(len(x)*0.55)
        fft_y_source.data.update(freq=x[1:axis_len], amplitude=power[1:axis_len])

#        fft_y_source.data.update(freq=x/2, amplitude=2.0/N * np.abs(y_fft[:N//2]))


        # Intensity FFT
        # i_fft = fft(intensities)
        # i_freqs = fftfreq(N, T)[:N//2]
        x=np.arange(0,np.ceil(100),np.ceil(100)/len(intensities))
        power = np.abs(np.fft.fft(intensities))**2
        axis_len = int(len(x)*0.55)
        fft_i_source.data.update(freq=x[1:axis_len], amplitude=power[1:axis_len])

    def device_select_callback(_attr, _old, new):
        nonlocal device_name, device_channels
        device_name = new
        device_channels = f"{new}:XPOS", f"{new}:YPOS", f"{new}:INTENSITY"

        # reset figures
        buffer.clear()
        doc.add_next_tick_callback(_update_plots)

    device_select = Select(title="Device:", options=DEVICES)
    device_select.on_change("value", device_select_callback)
    device_select.value = DEVICES[0]

    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=1000, step=100, low=100)

    update_plots_periodic_callback = None

    def update_toggle_callback(_attr, _old, new):
        nonlocal update_plots_periodic_callback
        if new:
            thread = Thread(target=_collect_data)
            thread.start()

            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 1000)

            xpos_ch, ypos_ch, i0_ch = device_channels

            xy_fig.xaxis.axis_label = xpos_ch
            xy_fig.yaxis.axis_label = ypos_ch
            ix_fig.xaxis.axis_label = i0_ch
            ix_fig.yaxis.axis_label = xpos_ch
            iy_fig.xaxis.axis_label = i0_ch
            iy_fig.yaxis.axis_label = ypos_ch

            device_select.disabled = True
            num_shots_spinner.disabled = True
            push_elog_button.disabled = True

            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:
            doc.remove_periodic_callback(update_plots_periodic_callback)

            device_select.disabled = False
            num_shots_spinner.disabled = False
            push_elog_button.disabled = False

            update_toggle.label = "Update"
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)

    def push_elog_button_callback():
        msg_id = push_elog(
            figures=((fig_layout, "jitter.png"),),
            message="",
            attributes={
                "Author": "sf-photodiag",
                "Entry": "Info",
                "Domain": "ARAMIS",
                "System": "Diagnostics",
                "Title": f"{device_name} jitter",
            },
        )
        log.info(
            f"Logbook entry created for {device_name} jitter: "
            f"https://elog-gfa.psi.ch/SF-Photonics-Data/{msg_id}"
        )

    push_elog_button = Button(label="Push elog")
    push_elog_button.on_click(push_elog_button_callback)

    fig_layout = gridplot([[xy_fig, ix_fig, iy_fig], [fft_x_fig, fft_y_fig, fft_i_fig]], toolbar_options={"logo": None})
    tab_layout = column(
        fig_layout,
        row(
            device_select,
            num_shots_spinner,
            column(Spacer(height=18), row(update_toggle, push_elog_button)),
        ),
    )

    return TabPanel(child=tab_layout, title="jitter/FFT")