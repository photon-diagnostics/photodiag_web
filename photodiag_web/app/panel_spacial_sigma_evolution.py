import asyncio
from collections import deque
from datetime import datetime
from functools import partial
from threading import Event, Thread
import bsread

import epics
import numpy as np
import scipy
from bokeh.layouts import column, row
from bokeh.models import (
    Button,
    ColumnDataSource,
    NumericInput,
    Select,
    Spacer,
    Spinner,
    TabPanel,
    TextInput,
    Toggle,
    Span,
    Legend,
)
from bokeh.plotting import curdoc, figure
from lmfit.models import GaussianModel

from cam_server_client.pipeline_client import PipelineClient, get_host_port_from_stream_address
from bsread import source, SUB


from photodiag_web import SPECT_DEV_LIST, epics_collect_data, get_device_domain, push_elog


def create(title):



    doc = curdoc()
    log = doc.logger


    buffer = deque()
    imageCut = []
    imageY = []
    image = []

    devices = SPECT_DEV_LIST

    pvs_x = {}

    for device in devices:
        device_root = device.split('/')[0]
        pvs_x[device] = epics.PV(f"{device_root}:SPECTRUM_X")

    update_plots_periodic_callback = None

    image_fig = figure(
        height=400, width=1000, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    image_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    image_fig.image(source=image_source, palette="Magma256",level="image")

    sigma_evolution_fig = figure(
        height=250, width=1000, x_axis_label="Slice Number", tools="pan,wheel_zoom,save,reset"
    )

    center_evolution_fig = figure(
        height=250, width=1000, x_axis_label="Slice Number", tools="pan,wheel_zoom,save,reset",y_range=(0, 2240)
    )

    sigma_evolution_source = ColumnDataSource(dict(x=[], y=[]))
    sigma_evolution_fig.line(source=sigma_evolution_source, legend_label="Sigma Evolution")

    center_evolution_source = ColumnDataSource(dict(x=[], y=[]))
    center_evolution_fig.line(source=center_evolution_source, legend_label="Center Evolution")
     
    parameter_time_evolution_fig = figure(
        height=250, width=1000, x_axis_label="Time[s]", x_axis_type="datetime", tools="pan,wheel_zoom,save,reset"
    )
    parameter_time_evolution_fig.add_layout(Legend(), 'right')
   
    parameter_time_evolution_source = ColumnDataSource(dict(x=[], param=[]))
   
    parameter_time_evolution_fig.line(
        source=parameter_time_evolution_source, y="param", line_color="red", legend_label="Weighted mean diffrence"
    )

    parameter_time_evolution_fig.legend.click_policy = "hide"
    parameter_time_evolution_fig.y_range.only_visible = True

    async def _update_plots():
        if len(image) < 1:    
            return
        axis = np.arange(image[0,:].size)
        if imageCut.size != 0:
                axis = imageCut
           
        image_source.data.update(
            image=[image**exp_spinner.value],
            x = [np.min(axis)],
            y = [0],
            dw= [np.max(axis) - np.min(axis)],
            dh = [image[:,0].size]
        )
        simgaArray, centerArray, center_weighted_average, weights = getSigmaEvolution(amount_of_slices_spinner.value)
        sigma_evolution_source.data.update(
            x = np.arange(amount_of_slices_spinner.value),
            y = simgaArray,
        )
        center_evolution_source.data.update(
            x = np.arange(amount_of_slices_spinner.value),
            y = centerArray
        )
        print(center_weighted_average)
        param = ((np.abs(centerArray - center_weighted_average))*weights).mean()/weights.sum()
        print(param)
        parameter_time_evolution_source.stream(
            dict(
                x=[datetime.now()],
                param = [param],
            ),
            rollover=300,
        )
       


    def _gauss_function(x,offset,amplitude,center,standardDeviation):
        return amplitude * np.exp(-(x-center)**2/(2*standardDeviation**2)) + offset


    def _gauss_deriv(x, offset, amplitude, center, standard_deviation):
        fac = np.exp(-(x - center) ** 2 / (2 * standard_deviation ** 2))
        result = np.empty((4, x.size), dtype=x.dtype)
        result[0, :] = 1.0
        result[1, :] = fac
        result[2, :] = amplitude * fac * (x - center) / (standard_deviation ** 2)
        result[3, :] = amplitude * fac * ((x - center) ** 2) / (standard_deviation ** 3)
        return result


    def device_select_callback(_attr, _old, new):
        # reset figures
        doc.add_next_tick_callback(_update_plots)
       # doc.add_next_tick_callback(_reset_calib_plot)

        # update default widget values

    
    def push_fit_elog_button_callback():
        device_name = device_select.value
        domain = get_device_domain(device_name)

        msg_id = push_elog(
            figures=(),
            message="Symmetry",
            attributes={
                "Author": "sf-photodiag",
                "Entry": "Info",
                "Domain": domain,
                "System": "Diagnostics",
                "Title": f"{device_name} Center analysis results",
            },
        )
        log.info(
            f"Logbook entry created for {device_name}: "
            f"https://elog-gfa.psi.ch/SF-Photonics-Data/{msg_id}"
        )

    push_fit_elog_button = Button(label="Push plot elog")
    push_fit_elog_button.on_click(push_fit_elog_button_callback)

    def update_toggle_callback(_attr, _old, new):
        nonlocal imageY
        nonlocal update_plots_periodic_callback
        pv_x = pvs_x[device_select.value] 
        if new:
            if pv_x.value.size != 0:
                imageY = pv_x.value
            else: 
               imageY = np.arange(image[0,:].size)
            thread = Thread(target=_collect_data)
            thread.start()


            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 3000)
#            doc.add_next_tick_callback(_live_lock_gui)

            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:

            doc.remove_periodic_callback(update_plots_periodic_callback)
    #       doc.add_next_tick_callback(_live_unlock_gui)

            update_toggle.label = "Update" 
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)



    def gauss_fit_psss(profile, axis, **kwargs):
        if axis.shape[0] != profile.shape[0]:
            raise RuntimeError("Invalid axis passed %d %d" % (axis.shape[0], profile.shape[0]))

        offset = kwargs.get('offset', profile.min())  # Minimum is good estimation of offset
        amplitude = kwargs.get('amplitude', profile.max() - offset)  # Max value is a good estimation of amplitude
        center = kwargs.get('center', np.dot(axis,
                                                profile) / profile.sum())  # Center of mass is a good estimation of center (mu)
        # Consider gaussian integral is amplitude * sigma * sqrt(2*pi)
        standard_deviation = kwargs.get('standard_deviation', scipy.integrate.trapz((profile - offset), x=axis) / (
                    amplitude * np.sqrt(2 * np.pi)))
        maxfev = kwargs.get('maxfev', 20)  # the default is 100 * (N + 1), which is over killing

        # If user requests fitting to be skipped, return the estimated parameters.
        if kwargs.get('skip', False):
            return offset, amplitude, center, abs(standard_deviation)

        try:
            optimal_parameter, _ = scipy.optimize.curve_fit(
                _gauss_function, axis, profile.astype("float64"),
                p0=[offset, amplitude, center, standard_deviation],
                jac=_gauss_deriv,
                col_deriv=1,
                maxfev=1000)
        except BaseException as e:
            #Make sure return always as same type
            optimal_parameter = np.array([offset, amplitude, center, standard_deviation]).astype("float64")

        offset, amplitude, center, standard_deviation = optimal_parameter
        return offset, amplitude, center, abs(standard_deviation)
    
    def getSigmaEvolution(slices):
        det = round(image[0,:].size/slices)
        sigma_evolution = np.zeros(slices)
        center_evolution = np.zeros(slices)
        weight = np.zeros(slices)
        tot = 0
        center_tot = 0
        for i in range(slices):
            chunk = np.mean(image[:,i*det:((i+1)*det)-1],axis=1)
            tot_chunk = chunk.sum()
            tot += tot_chunk 
            axis = np.arange(chunk.size)
            smoothed_spectrum = scipy.signal.savgol_filter(chunk, 51, 3)

            # check wether spectrum has only noise. the average counts per pixel at the peak
            # should be larger than 1.5 to be considered as having real signals.
            minimum, maximum = smoothed_spectrum.min(), smoothed_spectrum.max()
            amplitude = maximum - minimum
            skip = False
            offset, amplitude, center, sigma = gauss_fit_psss(chunk[::2],axis[::2],
                                                                    offset=minimum, amplitude=amplitude, skip=skip,
                                                                    maxfev=10)
            center_tot += tot_chunk * center
            np.put(sigma_evolution,[i],[sigma])
            np.put(center_evolution,[i],[center])
            np.put(weight,[i],[tot_chunk])
        return sigma_evolution, center_evolution, center_tot/tot,weight
    
    def cut_out_noise(data):
        nonlocal imageCut
        sum = np.sum(data,axis=0)
        max = np.max(sum)
        first = 0
        last = sum.size -1
        for i in range (sum.size):

            if sum[i]*threshHold_spinner.value>max and first == 0:
                first = i
            if sum[i]*threshHold_spinner.value>max:
                last = i
        imageCut = imageY[first:last]
        return data[:,first:last]

    



    def _collect_data():
        nonlocal image
        device_name = device_select.value
        device_name = device_name.split('/')[0]
        

        client = PipelineClient()
        pipeline_config = {"camera_name": "SARFE10-PSSS059",  "image_averaging":num_shots_spinner.value, "max_frame_rate":1,"image_good_region": {
         "threshold": 0.3,
         "gfscale": 1
        }}
        instance_id, pipeline_stream_address = client.create_instance_from_config(pipeline_config)
        # Extract the stream hostname and port from the stream address. 
        pipeline_host, pipeline_port = get_host_port_from_stream_address(pipeline_stream_address)

        with source(host=pipeline_host, port=pipeline_port, mode=SUB) as stream:
            while update_toggle.active:
        # Receive next message. 
                data = stream.receive()
                image = cut_out_noise(data.data.data["image"].value)

        #try:
         #   with bsread.source(channels=device_channels,dispatcher_url="https://dispatcher-api.psi.ch/sf-imagebuffer") as stream:
          #      while update_toggle.active:
           #         msg_data = stream.receive().data
            #        values = msg_data.data.get(f"{device_name}:SARFE10-PSSS059:FPICTURE").value
             #       buffer.append(values)

        #except Exception as e:
         #   log.error(f"Error collecting data: {e}")
       
    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=100, step=1, low=1)
    amount_of_slices_spinner = Spinner(title="Slices Amount:", mode="int", value=10, step=1, low=1)
    exp_spinner = Spinner(title="Exponent:", mode="float", value=1.0, step=0.05, low=0)
    threshHold_spinner = Spinner(title="Threshhold:", mode="float", value=1.2, step=0.05, low=1)

    device_select = Select(title="Device:", options=devices)
    device_select.on_change("value", device_select_callback)
    device_select.value = devices[0]

    fig_layout = column(sigma_evolution_fig,center_evolution_fig,image_fig,parameter_time_evolution_fig)
    tab_layout = column(
        fig_layout,
        row(
            device_select,
            num_shots_spinner,
            amount_of_slices_spinner,
            threshHold_spinner,
            exp_spinner,
            column(Spacer(height=18), update_toggle),
            column(Spacer(height=18), push_fit_elog_button),
        ),
    ) 


    return TabPanel(child=tab_layout, title=title)