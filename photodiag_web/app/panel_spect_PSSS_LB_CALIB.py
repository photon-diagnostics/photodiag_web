import asyncio
from collections import deque
from datetime import datetime
from functools import partial
from threading import Event, Thread
import time

import epics
import numpy as np
from bokeh.layouts import column, row
from bokeh.models import (
    Button,
    ColumnDataSource,
    NumericInput,
    Select,
    Spacer,
    Spinner,
    TabPanel,
    TextInput,
    Toggle,
)
from bokeh.plotting import curdoc, figure
from lmfit.models import GaussianModel

from photodiag_web import epics_collect_data, get_device_domain, push_elog,epics_collect_data

import UndulatorK as und


model = GaussianModel(prefix="g0_")



FWHM_TO_SIGMA = 1 / (2 * np.sqrt(2 * np.log(2)))  # ~= 1 / 2.355


def create(title):
    doc = curdoc()
    log = doc.logger
    Und = und.UndulatorK()

    params = model.make_params(
        g0_sigma=dict(value=100),
        g0_center=dict(value=1000),
        g0_amplitude=dict(value=1, min=0),
    )

# setup of move energy
    def move_button_callback():
        
        Und.get_current_k()
        try:
            new_energy = move_to.value
            Und.change_energy(new_energy, dummy=False)
        except Exception as e:
            log.error(e)

    move_button = Button(label="Move Aramis energy")
    move_button.on_click(move_button_callback)
    move_to = Spinner(title="Set FEL energy:", width= 100)
    move_to.value = Und.get_FEL_energy()

    # setup of calibration
    def calib_button_callback():
        scan_range = np.arange(from_spinner.value, to_spinner.value, step_spinner.value)
        numShots = 50
        #channels = [f"{device_name}:SPECTRUM_Y"]
        channels = ['SARFE10-PSSS059-LB:SPECTRUM_Y']
        doc.add_next_tick_callback(_reset_calib_plot)

        try:
            data_out =  energy_scan(scan_range, channels, numShots)
            print(data_out.shape)

        except Exception as e:
            log.error(e)

    calib_button = Button(label="Calibrate spectrometer energy axis")
    calib_button.on_click(calib_button_callback)

    # setup of energy scan and calib plot update
    from_spinner = Spinner(title="From:", width=100)
    from_spinner.value = Und.get_FEL_energy()-0.012
    to_spinner = Spinner(title="To:", width=100)
    to_spinner.value = Und.get_FEL_energy()+0.012
    step_spinner = Spinner(title="Step:", width=100)
    step_spinner.value = 0.004


    def energy_scan(scan_range, channels, numShots):
        print('In energy scan')
        scan_mean = []
        start_energy = Und.get_FEL_energy()
        #print('Starting FEL energy %2p'%start_energy)
        for pos in scan_range:
            Und.change_energy(pos, dummy=False)
            time.sleep(50)
            data = epics_collect_data(channels, numShots)
            
            this_data_mean = np.mean(data[0], axis =0)
            this_data_mean = this_data_mean/np.max(this_data_mean)
            scan_mean.append(this_data_mean)
     
            doc.add_next_tick_callback(partial(_update_calib_plot, pos, this_data_mean))

            # if stop_event.is_set():
            #     break
        
        Und.change_energy(start_energy, dummy=False)

        return np.asarray(scan_mean)

    async def _update_calib_plot(x, wf):
        print('In aysnc def')
        pixel_axis = np.arange(0,len(wf))
        print(pixel_axis)
        fit_result = model.fit(wf, params, x=pixel_axis)
        print(fit_result.values['g0_center'])
        calib_line_source.stream(dict(x=[x], y=[fit_result.values['g0_center']]))
        fit_line_source.data.update(x=pixel_axis, spec=wf, fit=fit_result.best_fit)

    async def _reset_calib_plot():
        calib_line_source.data.update(x=[], y=[])

    # setup of plots
     # calibration figure
    calib_fig = figure(
        height=500,
        width=500,
        x_axis_label="Set FEL energz [eV]",
        y_axis_label="Camera Gauss fit pixel",
        tools="pan,wheel_zoom,save,reset",
    )

    calib_line_source = ColumnDataSource(dict(x=[], y=[]))
    calib_fig.line(source=calib_line_source, y = 'y',line_color="red")
    calib_fig.toolbar.logo = None

    # fit figure
    fit_fig = figure(
        height=500,
        width=500,
        x_axis_label="Camera pixel",
        y_axis_label="Intensity [arb]",
        tools="pan,wheel_zoom,save,reset",
    )

    fit_line_source = ColumnDataSource(dict(x=[], spec=[], fit = []))

    fit_fig.line(source=fit_line_source, y = 'spec',line_color="purple",legend_label="Avg spec")
    fit_fig.line(source=fit_line_source, y = 'fit',line_color="red",legend_label="Gauss fit")
    fit_fig.toolbar.logo = None
    
    # set page layout
    tab_layout = column(
        calib_fig,
        fit_fig,
        row(move_to,
            column(Spacer(height=18), move_button),
        ),
        row(
            from_spinner,
            to_spinner,
            step_spinner,
            calib_button,
        )
    )

    return TabPanel(child=tab_layout, title=title)
