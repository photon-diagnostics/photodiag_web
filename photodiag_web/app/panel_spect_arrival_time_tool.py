from collections import deque
from datetime import datetime
from threading import Lock, Thread
import time

import bsread
import numpy as np
import epics
from bokeh.layouts import column, row, gridplot
from bokeh.models import Band, ColumnDataSource, Spacer, Span, Spinner, TabPanel, Toggle, Div, Button
from bokeh.plotting import curdoc, figure

def create():
    doc = curdoc()
    log = doc.logger

    # ------------------------------
    # Guide Text
    # ------------------------------
    guide_text = Div(text="""
    <div style='border: 2px solid #91d5ff; background-color: #e6f7ff; padding: 15px; border-radius: 8px;'>
      <h2 style='color:#005bb5;'>Bernina Arrival Time Panel</h2>
      <p style='font-size: 16px;'>
        This panel displays real-time arrival time data from the Bernina beamline.
        Data source are the SAROP21-ATT01 channels.
      </p>
    </div>
    """, styles={'margin-bottom': '15px'})

    # ------------------------------
    # Device Channels (Bernina)
    # ------------------------------
    device_channels = (
        "SAROP21-ATT01:signal",
        "SAROP21-ATT01:raw_wf",
        "SAROP21-ATT01:avg_dark_wf",
        "SAROP21-ATT01:xcorr",
        "SAROP21-ATT01:xcorr_ampl",
        "SAROP21-ATT01:edge_pos",
    )

    lock = Lock()

    # ------------------------------
    # Figures and Data Sources
    # ------------------------------
    # Signal Figure
    signal_fig = figure(
        height=250, width=1000,
        x_axis_label="Camera pixel",
        y_axis_label="Spectral intensity [arb]",
        tools="pan,wheel_zoom,save,reset"
    )
    signal_fig.toolbar.logo = None
    signal_source = ColumnDataSource(dict(x=[], y=[]))
    signal_fig.line(source=signal_source)

    # Waveform Figure
    waveform_fig = figure(
        height=250, width=1000,
        x_axis_label="Camera pixel",
        y_axis_label="Spectral intensity [arb]",
        tools="pan,wheel_zoom,save,reset"
    )
    waveform_fig.toolbar.logo = None
    waveform_source1 = ColumnDataSource(dict(x=[], y=[]))
    waveform_source2 = ColumnDataSource(dict(x=[], y=[]))
    waveform_fig.line(source=waveform_source1, legend_label="Raw waveform")
    waveform_fig.line(source=waveform_source2, line_color="black", legend_label="Avg dark waveform")

    # Cross Correlation Figure
    cross_corr_fig = figure(
        height=250, width=500,
        x_axis_label="Camera pixel",
        y_axis_label="Spectral cross corr intensity [arb]",
        tools="pan,wheel_zoom,save,reset"
    )
    cross_corr_fig.toolbar.logo = None
    cross_corr_source = ColumnDataSource(dict(x=[], y=[]))
    cross_corr_fig.line(source=cross_corr_source)

    # Edge Position Span (added to several figures)
    edge_span = Span(location=None, dimension="height", line_color="red", line_dash="dashed")
    signal_fig.add_layout(edge_span)
    waveform_fig.add_layout(edge_span)
    cross_corr_fig.add_layout(edge_span)

    # Edge Position Figure
    edge_pos_fig = figure(
        height=250, width=500,
        x_axis_label="Camera pixel",
        y_axis_label="Edge counts",
        tools="pan,wheel_zoom,save,reset"
    )
    edge_pos_fig.toolbar.logo = None
    edge_pos_quad_source = ColumnDataSource(dict(left=[], right=[], top=[]))
    edge_pos_fig.quad(source=edge_pos_quad_source, bottom=0)

    # Average Edge Position Figure
    edge_pos_avg_fig = figure(
        height=250, width=500,
        x_axis_label="Time", x_axis_type="datetime",
        y_axis_label="Avg edge position [pix]",
        tools="pan,wheel_zoom,save,reset"
    )
    edge_pos_avg_fig.toolbar.logo = None
    edge_pos_avg_source = ColumnDataSource(dict(x=[], y=[], lower=[], upper=[]))
    edge_pos_avg_fig.line(source=edge_pos_avg_source)
    edge_pos_avg_fig.add_layout(Band(base="x", source=edge_pos_avg_source))

    # Global Delay Figure
    global_delay_fig = figure(
        height=250, width=500,
        x_axis_label="Time", x_axis_type="datetime",
        y_axis_label="Global delay [mm]",
        tools="pan,wheel_zoom,save,reset"
    )
    global_delay_fig.toolbar.logo = None
    global_delay_source = ColumnDataSource(dict(x=[], y=[]))
    global_delay_fig.line(source=global_delay_source)
    global_delay_fig.add_layout(Band(base="x", source=global_delay_source))

    # Average Cross-Correlation Amplitude Figure
    xcorr_ampl_avg_fig = figure(
        height=250, width=500,
        x_axis_label="Time", x_axis_type="datetime",
        y_axis_label="Avg crosscorr ampl [arb]",
        tools="pan,wheel_zoom,save,reset"
    )
    xcorr_ampl_avg_fig.toolbar.logo = None
    xcorr_ampl_avg_source = ColumnDataSource(dict(x=[], y=[], lower=[], upper=[]))
    xcorr_ampl_avg_fig.line(source=xcorr_ampl_avg_source)
    xcorr_ampl_avg_fig.add_layout(Band(base="x", source=xcorr_ampl_avg_source))

    # FFT of Edge Position Figure
    edge_pos_fft_fig = figure(
        height=250, width=500,
        x_axis_label="Frequency [Hz]",
        y_axis_label="FFT [arb]",
        tools="pan,wheel_zoom,save,reset"
    )
    edge_pos_fft_fig.toolbar.logo = None
    edge_pos_fft_source = ColumnDataSource(dict(x=[], y=[]))
    edge_pos_fft_fig.line(source=edge_pos_fft_source)

    # ------------------------------
    # Data Buffers
    # ------------------------------
    data_cache = None
    buffer_edge_pos = deque()
    buffer_xcorr_ampl = deque()
    buffer_edge_pos_fft = deque(maxlen=1000)
    buffer_edge_pos_fft_pid = deque(maxlen=1000)

    # ------------------------------
    # Data Collection Thread
    # ------------------------------
    def _collect_data():
        nonlocal data_cache, buffer_edge_pos, buffer_xcorr_ampl
        data_cache = None
        buffer_edge_pos.clear()
        buffer_xcorr_ampl.clear()
        try:
            with bsread.source(channels=device_channels) as stream:
                while update_toggle.active:
                    msg_data = stream.receive().data
                    values = []
                    for ch in device_channels:
                        v = msg_data.data.get(ch)
                        values.append(v.value if v is not None else None)
                    with lock:
                        data_cache = values
                        if values[-1] is not None and values[-2] is not None:
                            buffer_edge_pos.append(values[-1])
                            buffer_xcorr_ampl.append(values[-2])
                            buffer_edge_pos_fft.append(values[-1])
                        else:
                            buffer_edge_pos_fft.append(np.nan)
                        buffer_edge_pos_fft_pid.append(msg_data.pulse_id)
        except Exception as e:
            log.error(e)

    # ------------------------------
    # Controls: Only a Spinner and an Update Toggle are included.
    # ------------------------------
    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=100, step=100, low=100)
    
    update_plots_periodic_callback = None
    update_toggle = Toggle(label="Update", button_type="primary")
    
    def update_toggle_callback(attr, old, new):
        nonlocal update_plots_periodic_callback
        if new:
            thread = Thread(target=_collect_data)
            thread.start()
            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 1000)
            num_shots_spinner.disabled = True
            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:
            doc.remove_periodic_callback(update_plots_periodic_callback)
            num_shots_spinner.disabled = False
            update_toggle.label = "Update"
            update_toggle.button_type = "primary"
    
    update_toggle.on_change("active", update_toggle_callback)

    # ------------------------------
    # Plot Update Callback (asynchronous)
    # ------------------------------
    async def _update_plots():
        if len(buffer_edge_pos) == 0:
            signal_source.data.update(x=[], y=[])
            waveform_source1.data.update(x=[], y=[])
            waveform_source2.data.update(x=[], y=[])
            cross_corr_source.data.update(x=[], y=[])
            edge_pos_quad_source.data.update(left=[], right=[], top=[])
            edge_pos_avg_source.data.update(x=[], y=[], lower=[], upper=[])
            xcorr_ampl_avg_source.data.update(x=[], y=[], lower=[], upper=[])
            global_delay_source.data.update(x=[], y=[])
            edge_pos_fft_source.data.update(x=[], y=[])
            return
        
        with lock:
            signal, raw_wf, avg_dark_wf, xcorr, _, last_edge_pos = data_cache
            buffer_edge_pos_cp = list(buffer_edge_pos)
            buffer_xcorr_ampl_cp = list(buffer_xcorr_ampl)
            buffer_edge_pos_fft_cp = list(buffer_edge_pos_fft)
            buffer_edge_pos_fft_pid_cp = list(buffer_edge_pos_fft_pid)
        
        edge_pos = np.array(buffer_edge_pos_cp)
        xcorr_ampl = np.array(buffer_xcorr_ampl_cp)
        counts, edges = np.histogram(edge_pos, bins=30)
        edge_pos_avg = np.mean(edge_pos)
        edge_pos_std = np.std(edge_pos)
        xcorr_ampl_avg = np.mean(xcorr_ampl)
        xcorr_ampl_std = np.std(xcorr_ampl)
        
        # FFT update
        edge_pos_fft = np.array(buffer_edge_pos_fft_cp)
        edge_pos_fft_pid = np.array(buffer_edge_pos_fft_pid_cp)
        nan_ind = np.isnan(edge_pos_fft)
        edge_pos_fft = edge_pos_fft[~nan_ind]
        edge_pos_fft_pid = edge_pos_fft_pid[~nan_ind]
        if len(edge_pos_fft_pid) > 0:
            edge_pos_fft_interp = np.interp(
                x=np.arange(min(edge_pos_fft_pid), max(edge_pos_fft_pid) + 1),
                xp=edge_pos_fft_pid,
                fp=edge_pos_fft,
            )
            fft_res = np.abs(np.fft.fft(edge_pos_fft_interp))
            freqs = np.fft.fftfreq(edge_pos_fft_interp.size, 1 / 100)
            fft_n = len(fft_res)
            fft_res = fft_res[1 : fft_n // 2]
            freqs = freqs[1 : fft_n // 2]
            edge_pos_fft_source.data.update(x=freqs, y=fft_res)
        
        if signal is None:
            signal_source.data.update(x=[], y=[])
        else:
            signal_source.data.update(x=np.arange(len(signal)), y=signal)
        
        if xcorr is None:
            cross_corr_source.data.update(x=[], y=[])
        else:
            len_diff = int(np.ceil((len(signal) - len(xcorr)) / 2))
            cross_corr_source.data.update(x=np.arange(len(xcorr)) + len_diff, y=xcorr)
        
        edge_pos_quad_source.data.update(left=edges[:-1], right=edges[1:], top=counts)
        edge_span.location = last_edge_pos
        
        waveform_source1.data.update(x=np.arange(len(raw_wf)), y=raw_wf)
        waveform_source2.data.update(x=np.arange(len(avg_dark_wf)), y=avg_dark_wf)
        
        stream_t = datetime.now()
        edge_pos_avg_source.stream(
            dict(
                x=[stream_t],
                y=[edge_pos_avg],
                lower=[edge_pos_avg - edge_pos_std],
                upper=[edge_pos_avg + edge_pos_std],
            ),
            rollover=3600,
        )
        xcorr_ampl_avg_source.stream(
            dict(
                x=[stream_t],
                y=[xcorr_ampl_avg],
                lower=[xcorr_ampl_avg - xcorr_ampl_std],
                upper=[xcorr_ampl_avg + xcorr_ampl_std],
            ),
            rollover=3600,
        )
        global_delay_source.stream(
            dict(
                x=[stream_t],
                y=[0],  # For Bernina, no delay feedback; set to 0 or remove updating
            ),
            rollover=3600,
        )
    
    # ------------------------------
    # Layout Arrangement
    # ------------------------------
    fig_layout = row(column(signal_fig, waveform_fig), column(cross_corr_fig, edge_pos_fig))
    additional_plots = row(edge_pos_avg_fig, xcorr_ampl_avg_fig, edge_pos_fft_fig)
    control_row = row(num_shots_spinner, update_toggle)
    tab_layout = column(
        guide_text,
        fig_layout,
        additional_plots,
        row(global_delay_fig),
        control_row,
    )
    
    return TabPanel(child=tab_layout, title="Bernina arrival time tool")
