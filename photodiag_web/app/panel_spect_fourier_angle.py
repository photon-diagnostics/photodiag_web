import asyncio
from collections import deque
from datetime import datetime
from functools import partial
from threading import Event, Thread
import bsread

import epics
import numpy as np
import scipy
from bokeh.layouts import column, row
from bokeh.models import (
    Button,
    ColumnDataSource,
    NumericInput,
    Select,
    Spacer,
    Spinner,
    TabPanel,
    TextInput,
    Toggle,
    Span,
    Legend,
)
from bokeh.plotting import curdoc, figure
from lmfit.models import GaussianModel

from cam_server_client.pipeline_client import PipelineClient, get_host_port_from_stream_address
from bsread import source, SUB
from scipy.ndimage import center_of_mass

from photodiag_web import SPECT_DEV_LIST, epics_collect_data, get_device_domain, push_elog
from scipy.fft import fft2, ifft2, fftshift


def create(title):



    doc = curdoc()
    log = doc.logger


    buffer = deque()
    imageY = []
    imageYCut = []
    image = []

    devices = SPECT_DEV_LIST

    pvs_x = {}

    for device in devices:
        device_root = device.split('/')[0]
        pvs_x[device] = epics.PV(f"{device_root}:SPECTRUM_X")

    update_plots_periodic_callback = None

    image_fig = figure(
        height=400, width=1000, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    image_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    image_fig.image(source=image_source, palette="Magma256",level="image")


    fourier_fig = figure(
        height=400, width=600, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    fourier_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    fourier_fig.image(source=fourier_source, palette="Magma256",level="image")


    parameter_time_evolution_fig = figure(
        height=250, width=1000, x_axis_label="Time[s]", x_axis_type="datetime", tools="pan,wheel_zoom,save,reset"
    )
    parameter_time_evolution_fig.add_layout(Legend(), 'right')
   
    parameter_time_evolution_source = ColumnDataSource(dict(x=[], angle=[]))

    parameter_time_evolution_fig.line(
        source=parameter_time_evolution_source, y="angle", line_color="red", legend_label="average Angle"
    )
    


    async def _update_plots():
        if len(image) < 1:    
            return
        axis = np.arange(image[0,:].size)
        image_source.data.update(
            image=[image],
            x = [np.min(axis)],
            y = [0],
            dw= [np.max(axis) - np.min(axis)],
            dh = [image[:,0].size]
        )
        fourier_array = cut_fourier_noice(fourier(image))
        fourier_source.data.update(
            image=[fourier_array],
            x = [np.min(axis)],
            y = [0],
            dw= [np.max(axis) - np.min(axis)],
            dh = [image[:,0].size]
        )
        a = center_of_mass(fourier_array[:,0:(round(fourier_array.shape[1]/2))-1])
        b = center_of_mass(fourier_array[:,(round(fourier_array.shape[1]/2)-1):(fourier_array.shape[1]-1)])

        parameter_time_evolution_source.stream(
            dict(
                x=[datetime.now()],
                angle = [np.arctan(-(a[0]-b[0])/(a[1]-b[1]-(round(fourier_array.shape[1]/2)-1)))*360/np.pi/2],
            ),
            rollover=300,
        )



    def device_select_callback(_attr, _old, new):
        # reset figures
        doc.add_next_tick_callback(_update_plots)
       # doc.add_next_tick_callback(_reset_calib_plot)

        # update default widget values

    
    def push_fit_elog_button_callback():
        device_name = device_select.value
        domain = get_device_domain(device_name)

        msg_id = push_elog(
            figures=(),
            message="Symmetry",
            attributes={
                "Author": "sf-photodiag",
                "Entry": "Info",
                "Domain": domain,
                "System": "Diagnostics",
                "Title": f"{device_name} Symmetry results",
            },
        )
        log.info(
            f"Logbook entry created for {device_name}: "
            f"https://elog-gfa.psi.ch/SF-Photonics-Data/{msg_id}"
        )

    push_fit_elog_button = Button(label="Push plot elog")
    push_fit_elog_button.on_click(push_fit_elog_button_callback)

    def update_toggle_callback(_attr, _old, new):
        nonlocal imageY
        nonlocal update_plots_periodic_callback
        pv_x = pvs_x[device_select.value] 
        if new:
            imageY = pv_x.value
            thread = Thread(target=_collect_data)
            thread.start()


            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 3000)
#            doc.add_next_tick_callback(_live_lock_gui)

            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:

            doc.remove_periodic_callback(update_plots_periodic_callback)
    #       doc.add_next_tick_callback(_live_unlock_gui)

            update_toggle.label = "Update" 
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)



    def fourier(data_in):
        fourier_transform = fft2(data_in**0.2)
        fourier_transform_shifted = fftshift(fourier_transform)
        magnitude_spectrum = np.log(np.abs(fourier_transform_shifted) + 1)
        return magnitude_spectrum
    
    def cut_fourier_noice(image):
        maximum = np.max(image)
        for i in range(image.shape[0]):
            for j in range(image.shape[1]):
                if(image[i][j]< threshHold_spinner.value*maximum):
                    image[i][j] = 0
        return image
    
    def cut_out_noise(data):
        nonlocal imageYCut
        sum = np.sum(data,axis=0)
        max = np.max(sum)
        first = 0
        last = sum.size -1
        for i in range (sum.size):

            if sum[i]*threshHold_spinner.value>max and first == 0:
                first = i
            if sum[i]*threshHold_spinner.value>max:
                last = i
        imageYCut = imageY[first:last]
        return data[:,first:last]**exp_spinner.value

    



    def _collect_data():
        nonlocal image
        buffer = deque(maxlen=num_shots_spinner.value)
        device_name = device_select.value
        device_name = device_name.split('/')[0]
        device_channels = f"{device_name}:FPICTURE"

        client = PipelineClient()
        pipeline_config = {"camera_name": "SARFE10-PSSS059",  "image_averaging":num_shots_spinner.value, "max_frame_rate":1}
        instance_id, pipeline_stream_address = client.create_instance_from_config(pipeline_config)
        # Extract the stream hostname and port from the stream address. 
        pipeline_host, pipeline_port = get_host_port_from_stream_address(pipeline_stream_address)

        with source(host=pipeline_host, port=pipeline_port, mode=SUB) as stream:
            while update_toggle.active:
        # Receive next message. 
                data = stream.receive()
                image = data.data.data["image"].value

        #try:
         #   with bsread.source(channels=device_channels,dispatcher_url="https://dispatcher-api.psi.ch/sf-imagebuffer") as stream:
          #      while update_toggle.active:
           #         msg_data = stream.receive().data
            #        values = msg_data.data.get(f"{device_name}:SARFE10-PSSS059:FPICTURE").value
             #       buffer.append(values)

        #except Exception as e:
         #   log.error(f"Error collecting data: {e}")
       
    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=1, step=1, low=1)
    threshHold_spinner = Spinner(title="Threshhold:", mode="float", value=0.4, step=0.05, low=0)
    exp_spinner = Spinner(title="Exponent:", mode="float", value=1.0, step=0.05, low=0)

    device_select = Select(title="Device:", options=devices)
    device_select.on_change("value", device_select_callback)
    device_select.value = devices[0]

    fig_layout = column(image_fig,fourier_fig,parameter_time_evolution_fig)
    tab_layout = column(
        fig_layout,
        row(
            device_select,
            num_shots_spinner,
            threshHold_spinner,
            exp_spinner,
            column(Spacer(height=18), update_toggle),
            column(Spacer(height=18), push_fit_elog_button),
        ),
    ) 


    return TabPanel(child=tab_layout, title=title)