import asyncio
from collections import deque
from datetime import datetime
from functools import partial
from threading import Event, Thread
import bsread

import epics
import numpy as np
import scipy
from bokeh.layouts import column, row
from bokeh.models import (
    Button,
    ColumnDataSource,
    NumericInput,
    Select,
    Spacer,
    Spinner,
    TabPanel,
    TextInput,
    Toggle,
    Span,
    Legend,
)
from bokeh.plotting import curdoc, figure
from lmfit.models import GaussianModel

from cam_server_client.pipeline_client import PipelineClient, get_host_port_from_stream_address
from bsread import source, SUB


from photodiag_web import SPECT_DEV_LIST, epics_collect_data, get_device_domain, push_elog
from scipy.fft import fft2, ifft2, fftshift


def create(title):



    doc = curdoc()
    log = doc.logger


    buffer = deque()
    imageY = []
    imageYCut = []
    image = []

    devices = SPECT_DEV_LIST

    pvs_x = {}

    for device in devices:
        device_root = device.split('/')[0]
        pvs_x[device] = epics.PV(f"{device_root}:SPECTRUM_X")

    update_plots_periodic_callback = None

    image_fig = figure(
        height=400, width=1000, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    image_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    image_fig.image(source=image_source, palette="Magma256",level="image")


    rec_high_fig = figure(
        height=400, width=600, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    rec_high_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    rec_high_fig.image(source=rec_high_source, palette="Magma256",level="image")

    cut_low_fig = figure(
        height=400, width=600, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    cut_low_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    cut_low_fig.image(source=cut_low_source, palette="Magma256",level="image")

    rec_low_fig = figure(
        height=400, width=600, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    rec_low_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    rec_low_fig.image(source=rec_low_source, palette="Magma256",level="image")


    cut_high_fig = figure(
        height=400, width=600, x_axis_label="Energy", tools="pan,wheel_zoom,save,reset"
    )
    cut_high_source = ColumnDataSource(dict(image=[],x=[],y=[],dh=[],dw=[]))
    cut_high_fig.image(source=cut_high_source, palette="Magma256",level="image")
    
    


    async def _update_plots():
        if len(image) < 1:    
            return
        axis = np.arange(image[0,:].size)
        if imageYCut.size != 0:
                axis = imageYCut
        image_source.data.update(
            image=[image],
            x = [np.min(axis)],
            y = [0],
            dw= [np.max(axis) - np.min(axis)],
            dh = [image[:,0].size]
        )
        freq_cuts(image, cut_size_spinner.value, axis,exp = 0.02)


    def device_select_callback(_attr, _old, new):
        # reset figures
        doc.add_next_tick_callback(_update_plots)
       # doc.add_next_tick_callback(_reset_calib_plot)

        # update default widget values

    
    def push_fit_elog_button_callback():
        device_name = device_select.value
        domain = get_device_domain(device_name)

        msg_id = push_elog(
            figures=(),
            message="Symmetry",
            attributes={
                "Author": "sf-photodiag",
                "Entry": "Info",
                "Domain": domain,
                "System": "Diagnostics",
                "Title": f"{device_name} Symmetry results",
            },
        )
        log.info(
            f"Logbook entry created for {device_name}: "
            f"https://elog-gfa.psi.ch/SF-Photonics-Data/{msg_id}"
        )

    push_fit_elog_button = Button(label="Push plot elog")
    push_fit_elog_button.on_click(push_fit_elog_button_callback)

    def update_toggle_callback(_attr, _old, new):
        nonlocal imageY
        nonlocal update_plots_periodic_callback
        pv_x = pvs_x[device_select.value] 
        if new:
            imageY = pv_x.value
            thread = Thread(target=_collect_data)
            thread.start()


            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 3000)
#            doc.add_next_tick_callback(_live_lock_gui)

            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:

            doc.remove_periodic_callback(update_plots_periodic_callback)
    #       doc.add_next_tick_callback(_live_unlock_gui)

            update_toggle.label = "Update" 
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)



    def freq_cuts(data_in, width, e_axis,exp = 1):
        hor_axis = np.arange(0,data_in.shape[0])
        data_in= data_in**0.2
        
        fourier_transform = fft2(data_in)

        # Shift the zero frequency components to the center
        fourier_transform_shifted_all = fftshift(fourier_transform)
        fourier_transform_shifted_high = fftshift(fourier_transform)
        fourier_transform_shifted_low = fftshift(fourier_transform)

        # find index of magnitude_spectrum max
        magnitude_spectrum = np.log(np.abs(fourier_transform_shifted_all) + 1)
        ind_max = np.argmax(magnitude_spectrum.sum(axis=0))
        
        # Compute the 2D Fourier Transform

        fourier_transform_shifted_low[:,0:int(ind_max-width/2)]=0
        fourier_transform_shifted_low[:,int(ind_max+width/2):]=0
        fourier_transform_shifted_high[:,int(ind_max-width/2):int(ind_max+width/2)]=0

        # calculate the different magnitude spectra
        magnitude_spectrum_high = np.log(np.abs(fourier_transform_shifted_high) + 1)
        magnitude_spectrum_low = np.log(np.abs(fourier_transform_shifted_low) + 1)
        
        # reconstruct image
        reconstructed_image_high = np.abs(ifft2(fftshift(fourier_transform_shifted_high)))
        reconstructed_image_low = np.abs(ifft2(fftshift(fourier_transform_shifted_low)))
        
        # spectra (sum in horizontal axis
        spec = data_in.sum(axis=0)
        spec = spec-np.mean(spec[0:10])
        spec = spec/np.max(spec)

        spec_low = reconstructed_image_low.sum(axis=0)
        spec_low = spec_low-np.mean(spec_low[0:10])
        spec_low = spec_low/np.max(spec_low)

        spec_high = reconstructed_image_high.sum(axis=0)
        spec_high = spec_high-np.mean(spec_high[0:10])
        spec_high = spec_high/np.max(spec_high)
        rec_high_source.data.update(
            image=[reconstructed_image_high],
            x = [np.min(e_axis)],
            y = [0],
            dw= [np.max(e_axis) - np.min(e_axis)],
            dh = [hor_axis.size]
        )

        cut_high_source.data.update(
            image=[magnitude_spectrum_high],
            x = [np.min(e_axis)],
            y = [0],
            dw= [np.max(e_axis) - np.min(e_axis)],
            dh = [hor_axis.size]
        )
        rec_low_source.data.update(
            image=[reconstructed_image_low],
            x = [np.min(e_axis)],
            y = [0],
            dw= [np.max(e_axis) - np.min(e_axis)],
            dh = [hor_axis.size]
        )
        cut_low_source.data.update(
            image=[magnitude_spectrum_low],
            x = [np.min(e_axis)],
            y = [0],
            dw= [np.max(e_axis) - np.min(e_axis)],
            dh = [hor_axis.size]
        )
    
    def cut_out_noise(data):
        nonlocal imageYCut
        sum = np.sum(data,axis=0)
        max = np.max(sum)
        first = 0
        last = sum.size -1
        for i in range (sum.size):

            if sum[i]*threshHold_spinner.value>max and first == 0:
                first = i
            if sum[i]*threshHold_spinner.value>max:
                last = i
        imageYCut = imageY[first:last]
        return data[:,first:last]**exp_spinner.value

    



    def _collect_data():
        nonlocal image
        buffer = deque(maxlen=num_shots_spinner.value)
        device_name = device_select.value
        device_name = device_name.split('/')[0]
        device_channels = f"{device_name}:FPICTURE"

        client = PipelineClient()
        pipeline_config = {"camera_name": "SARFE10-PSSS059",  "image_averaging":num_shots_spinner.value, "max_frame_rate":1,"image_good_region": {
         "threshold": 0.3,
         "gfscale": 1
        }}
        instance_id, pipeline_stream_address = client.create_instance_from_config(pipeline_config)
        # Extract the stream hostname and port from the stream address. 
        pipeline_host, pipeline_port = get_host_port_from_stream_address(pipeline_stream_address)

        with source(host=pipeline_host, port=pipeline_port, mode=SUB) as stream:
            while update_toggle.active:
        # Receive next message. 
                data = stream.receive()
                image = cut_out_noise(data.data.data["image"].value)

        #try:
         #   with bsread.source(channels=device_channels,dispatcher_url="https://dispatcher-api.psi.ch/sf-imagebuffer") as stream:
          #      while update_toggle.active:
           #         msg_data = stream.receive().data
            #        values = msg_data.data.get(f"{device_name}:SARFE10-PSSS059:FPICTURE").value
             #       buffer.append(values)

        #except Exception as e:
         #   log.error(f"Error collecting data: {e}")
       
    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=1, step=1, low=1)
    cut_size_spinner = Spinner(title="Cut width:", mode="int", value=14, step=1, low=1)
    threshHold_spinner = Spinner(title="Threshhold:", mode="float", value=1.2, step=0.05, low=1)
    exp_spinner = Spinner(title="Exponent:", mode="float", value=1.0, step=0.05, low=0)

    device_select = Select(title="Device:", options=devices)
    device_select.on_change("value", device_select_callback)
    device_select.value = devices[0]

    fig_layout = column(image_fig,row(rec_high_fig,cut_high_fig),row(rec_low_fig,cut_low_fig))
    tab_layout = column(
        fig_layout,
        row(
            device_select,
            num_shots_spinner,
            cut_size_spinner,
            threshHold_spinner,
            exp_spinner,
            column(Spacer(height=18), update_toggle),
            column(Spacer(height=18), push_fit_elog_button),
        ),
    ) 


    return TabPanel(child=tab_layout, title=title)