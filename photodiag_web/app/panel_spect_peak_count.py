from collections import deque
from threading import Thread
import random
import bsread
import numpy as np
from bokeh.layouts import column, row
from bokeh.models import ColumnDataSource, Select, Spacer, TabPanel, Toggle
from bokeh.plotting import curdoc, figure
from scipy.signal import find_peaks, savgol_filter
import tensorflow as tf
import os

# ##### For Saving data on the local machine #### 
# results_folder = '/home/schoel_m/results_app'
# if not os.path.exists(results_folder):
#     os.makedirs(results_folder)
#
# num_peaks_file = os.path.join(results_folder, 'num_peaks.txt')
# found_peaks_file = os.path.join(results_folder, 'found_peaks.txt')
#
# ########################

def interpolate_spectrum(spec_x, spec_y, n_points=1000):
    """
    Resample the spectrum (spec_y) defined on the energy axis (spec_x)
    to exactly n_points using linear interpolation.
    Returns the new energy axis and interpolated intensity data.
    """
    new_spec_x = np.linspace(np.min(spec_x), np.max(spec_x), n_points)
    new_spec_y = np.interp(new_spec_x, spec_x, spec_y)
    return new_spec_x, new_spec_y

def create(title, devices):
    doc = curdoc()
    log = doc.logger

    device_name = ""
    device_channels = ("", "")

    # single shot spectrum figure
    single_shot_fig = figure(
        height=250, width=1000, x_axis_label="Photon energy [eV]", tools="pan,wheel_zoom,save,reset"
    )
    single_shot_fig.toolbar.logo = None

    single_shot_line_source = ColumnDataSource(dict(x=[], y=[]))
    single_shot_fig.line(source=single_shot_line_source, legend_label="Spectrum")

    single_shot_smooth_line_source = ColumnDataSource(dict(x=[], y=[]))
    single_shot_fig.line(
        source=single_shot_smooth_line_source, line_color="orange", legend_label="Smoothed"
    )

    # gradient figure
    gradient_fig = figure(
        height=250, width=1000, x_axis_label="Photon energy [eV]", tools="pan,wheel_zoom,save,reset"
    )
    gradient_fig.toolbar.logo = None

    gradient_line_source = ColumnDataSource(dict(x=[], y=[]))
    gradient_fig.line(source=gradient_line_source, legend_label="Abs of gradient")

    peak_scatter_source = ColumnDataSource(dict(x=[], y=[]))
    # gradient_fig.circle(source=peak_scatter_source, color="orange", size=10, legend_label="Peaks")

    # number of peaks distribution figure
    num_peaks_dist_fig = figure(
        height=500,
        width=500,
        x_axis_label="Number of predicted peaks",
        y_axis_label="Number of shots",
        tools="pan,wheel_zoom,save,reset",
    )
    num_peaks_dist_fig.toolbar.logo = None

    num_peaks_dist_quad_source = ColumnDataSource(dict(left=[], right=[], top=[]))
    num_peaks_dist_fig.quad(source=num_peaks_dist_quad_source, bottom=0)

    # number of peaks over time figure
    num_peaks_over_time_fig = figure(
        height=250, width=1000, x_axis_label="Time", y_axis_label="Number of Peaks", tools="pan,wheel_zoom,save,reset"
    )
    num_peaks_over_time_fig.toolbar.logo = None

    num_peaks_over_time_source = ColumnDataSource(dict(x=[], y_predicted=[], y_found=[]))
    num_peaks_over_time_fig.line(source=num_peaks_over_time_source, line_color="blue", legend_label="Predicted Peaks", y='y_predicted')
    # Added line for found peaks:
    num_peaks_over_time_fig.line(source=num_peaks_over_time_source, line_color="orange", legend_label="Found Peaks", y='y_found')

    single_shot_cache = [[], [], [], [], 0]
    buffer_num_peaks = deque()
    buffer_found_peaks = deque()

    # Load the machine learning model
    saved_model_path = '/home/arrell_c/photodiag_web/photodiag_web/app/modeldata/saved_model'
    model = tf.saved_model.load(saved_model_path)
    infer = model.signatures['serve']
    #infer = model.signatures['serving_default']

    def _collect_data():
        nonlocal single_shot_cache, buffer_num_peaks, buffer_found_peaks
        single_shot_cache = [[], [], [], [], 0]
        buffer_num_peaks = deque()
        buffer_found_peaks = deque()

        # Default parameters
        peak_dist = 100
        peak_height = 0.009

        try:
            with bsread.source(channels=device_channels) as stream:
                while update_toggle.active:
                    msg_data = stream.receive().data
                    values = [msg_data.data.get(ch).value for ch in device_channels]
                    if not any(val is None for val in values):
                        spec_x, spec_y = values
                        # Normalize the spectrum
                        spec_y = spec_y / np.max(spec_y)
                        # Smooth using Savitzky–Golay filter
                        spec_y_smoothed = savgol_filter(spec_y, 51, 3)
                        # Calculate the gradient of the smoothed data
                        spec_y_grad = np.abs(np.gradient(spec_y_smoothed))
                        # Find peaks on the gradient
                        peaks, _ = find_peaks(spec_y_grad, distance=peak_dist, height=peak_height)

                        # Store the processed data in cache for plotting
                        single_shot_cache = [spec_x, spec_y, spec_y_smoothed, spec_y_grad, peaks]

                        # Interpolate the spectrum to exactly 1000 points (preserving full spectral data)
                        new_spec_x, spec_y_interpolated = interpolate_spectrum(spec_x, spec_y, n_points=1000)
                        
                        # Use the interpolated spectrum for the model input
                        spec_y_input = spec_y_interpolated.reshape(1, 1000, 1).astype(np.float32)
                        #spec_y_input = spec_y.reshape(1, len(spec_y), 1).astype(np.float32)

                        prediction = infer(tf.constant(spec_y_input))
                        predicted_peaks = np.argmax(prediction['output'].numpy(), axis=1)[0]
                        #predicted_class = np.argmax(prediction['dense_1'].numpy(), axis=1)[0]
                        #predicted_peaks = 20 + predicted_class * 5
                        #predicted_peaks = 40 + predicted_class

                        # Use the actual count of peaks from find_peaks
                        found_peaks = len(peaks)
                        
                        # Optional: adjust threshold if needed
                        if found_peaks < 2:
                            predicted_peaks = 0

                        # Append the results to the buffers
                        buffer_found_peaks.append(found_peaks)
                        buffer_num_peaks.append(predicted_peaks)

        except Exception as e:
            log.error(e)

    update_plots_periodic_callback = None

    def update_toggle_callback(_attr, _old, new):
        nonlocal update_plots_periodic_callback
        if new:
            thread = Thread(target=_collect_data)
            thread.start()

            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 1000)

            device_select.disabled = True

            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:
            doc.remove_periodic_callback(update_plots_periodic_callback)

            device_select.disabled = False

            update_toggle.label = "Update"
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)

    async def _update_plots():
        if len(buffer_num_peaks) < 3 or len(buffer_found_peaks) < 3:
            num_peaks_dist_quad_source.data.update(left=[], right=[], top=[])
            return

        spec_x, spec_y, spec_y_smoothed, spec_y_grad, peaks = single_shot_cache

        num_peaks = np.array(buffer_num_peaks)
        num_peaks = abs(num_peaks)
        # Filter out zeros from the predicted peaks before computing the histogram.
        #num_peaks_filtered = num_peaks[num_peaks != 0]
        if len(num_peaks_filtered) > 0:
            counts, edges = np.histogram(num_peaks_filtered)
        else:
            counts, edges = np.array([]), np.array([])

        found_peaks = np.array(buffer_found_peaks)
        found_peaks = abs(found_peaks)

        # update glyph sources
        single_shot_line_source.data.update(x=spec_x, y=spec_y)
        single_shot_smooth_line_source.data.update(x=spec_x, y=spec_y_smoothed)
        gradient_line_source.data.update(x=spec_x, y=spec_y_grad)
        peak_scatter_source.data.update(x=spec_x[peaks], y=spec_y_grad[peaks])
        num_peaks_dist_quad_source.data.update(left=edges[:-1], right=edges[1:], top=counts)

        # Update the number of peaks over time
        current_time = len(num_peaks_over_time_source.data['x']) + 1
        num_peaks_over_time_source.stream(dict(x=[current_time], y_predicted=[num_peaks[-1]], y_found=[found_peaks[-1]]))
        
        # ##### Saving the latest num_peaks and found_peaks to text files in local folder ######
        # with open(num_peaks_file, 'a') as np_file:
        #     np_file.write(f"{current_time}: {num_peaks[-1]}\n")
        #
        # with open(found_peaks_file, 'a') as fp_file:
        #     fp_file.write(f"{current_time}: {found_peaks[-1]}\n")
        # ########################################
        
    def device_select_callback(_attr, _old, new):
        nonlocal device_name, device_channels, single_shot_cache
        device_name = new
        device_channels = f"{device_name}:SPECTRUM_X", f"{device_name}:SPECTRUM_Y"

        # reset figures and buffers
        single_shot_cache = [[], [], [], [], 0]
        buffer_num_peaks.clear()
        buffer_found_peaks.clear()
        doc.add_next_tick_callback(_update_plots)

    device_select = Select(title="Device:", options=devices)
    device_select.on_change("value", device_select_callback)
    device_select.value = devices[0]

    fig_layout = row(column(single_shot_fig, gradient_fig), num_peaks_dist_fig)
    tab_layout = column(
        fig_layout,
        row(
            device_select,
            column(Spacer(height=18), update_toggle),
        ),
    )

    final_layout = column(
        tab_layout,  # This contains the controls and original figures
        num_peaks_over_time_fig  # Add the number of peaks over time plot
    )

    return TabPanel(child=final_layout, title=title)
