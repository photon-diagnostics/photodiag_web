import asyncio
from collections import deque
from datetime import datetime
from functools import partial
from threading import Event, Thread
import bsread

import epics
import numpy as np
import scipy
from bokeh.layouts import column, row
from bokeh.models import (
    Button,
    ColumnDataSource,
    NumericInput,
    Select,
    Spacer,
    Spinner,
    TabPanel,
    TextInput,
    Toggle,
    Span,
    Legend,
)
from bokeh.plotting import curdoc, figure
from lmfit.models import GaussianModel

from photodiag_web import SPECT_DEV_LIST, epics_collect_data, get_device_domain, push_elog

def create(title):
    doc = curdoc()
    log = doc.logger


    buffer = deque()
    sigmaBuffer = deque()
    devices = SPECT_DEV_LIST

    pvs_x = {}
    pvs_y = {}
    mean = 6400
    median = 1000
    for device in devices:
        device_root = device.split('/')[0]
        pvs_x[device] = epics.PV(f"{device_root}:SPECTRUM_X")
        pvs_y[device] = epics.PV(f"{device_root}:SPECTRUM_Y")
        
 
    parameter_time_evolution_fig = figure(
        height=250, width=1000, x_axis_label="Time[s]", x_axis_type="datetime", tools="pan,wheel_zoom,save,reset"
    )
    parameter_time_evolution_fig.add_layout(Legend(), 'right')
   
    parameter_time_evolution_source = ColumnDataSource(dict(x=[], skew=[]))
    """
    parameter_time_evolution_fig.line(
        source=parameter_time_evolution_source, y="difference", line_color="green", legend_label="Mean - Median (Average)"
    )
    """
    parameter_time_evolution_fig.line(
        source=parameter_time_evolution_source, y="skew", line_color="red", legend_label="Skew (Average)"
    )
    """
    parameter_time_evolution_fig.line(
        source=parameter_time_evolution_source, y="single_difference", line_color="blue", legend_label="Mean - Median (Single Shot)"
    )
    parameter_time_evolution_fig.line(
        source=parameter_time_evolution_source, y="single_skew", line_color="orange", legend_label="Skew (Single Shot)"
    )
    """
    parameter_time_evolution_fig.legend.click_policy = "hide"
    parameter_time_evolution_fig.y_range.only_visible = True
    symmetry_fig = figure(
        height=250, width=1000, x_axis_label="Photon energy [eV]", tools="pan,wheel_zoom,save,reset"
    )
    single_shot_mean_line = Span(location=mean, dimension='height', line_color='red', line_width=1)
    single_shot_median_line = Span(location=median, dimension='height', line_color='green', line_width=1)

    single_shot_line_source = ColumnDataSource(dict(x=[], y=[]))
    symmetry_fig.line(source=single_shot_line_source, legend_label="Spectrum")
    symmetry_fig.renderers.extend([single_shot_mean_line, single_shot_median_line])

    symmetry_mean_fig = figure(
        height=250, width=1000, x_axis_label="Photon energy [eV]", tools="pan,wheel_zoom,save,reset"
    )
    average_mean_line = Span(location=mean, dimension='height', line_color='red', line_width=1)
    average_median_line = Span(location=median, dimension='height', line_color='green', line_width=1)

    mean_shot_line_source = ColumnDataSource(dict(x=[], y=[]))
    symmetry_mean_fig.line(source=mean_shot_line_source, legend_label="Spectrum")
    symmetry_mean_fig.renderers.extend([average_mean_line, average_median_line])



    update_plots_periodic_callback = None

    def _collect_data():
        nonlocal buffer
        nonlocal sigmaBuffer 
        buffer = deque(maxlen=num_shots_spinner.value)
        sigmaBuffer = deque(maxlen=num_shots_spinner.value)
        device_name = device_select.value
        device_name = device_name.split('/')[0]
        device_channels = f"{device_name}:SPECTRUM_X", f"{device_name}:SPECTRUM_Y",f"{device_name}:FIT-RMS"

        try:
            with bsread.source(channels=device_channels) as stream:
                while update_toggle.active:
                    msg_data = stream.receive().data
                    values = msg_data.data.get(f"{device_name}:SPECTRUM_Y").value
                    sigmaBuffer.append(msg_data.data.get(f"{device_name}:FIT-RMS").value)
                    buffer.append(values)

        except Exception as e:
            log.error(f"Error collecting data: {e}")

    def update_toggle_callback(_attr, _old, new):
        nonlocal update_plots_periodic_callback
        pv_x = pvs_x[device_select.value]
        pv_y = pvs_y[device_select.value]
        if new:
            thread = Thread(target=_collect_data)
            thread.start()
            value = pv_x.value

 #           pv_x.add_callback(update_x)
#            pv_y.add_callback(update_y)

            update_plots_periodic_callback = doc.add_periodic_callback(_update_plots, 3000)
#            doc.add_next_tick_callback(_live_lock_gui)

            update_toggle.label = "Stop"
            update_toggle.button_type = "success"
        else:
            pv_x.clear_callbacks()
            pv_y.clear_callbacks()

            doc.remove_periodic_callback(update_plots_periodic_callback)
    #       doc.add_next_tick_callback(_live_unlock_gui)

            update_toggle.label = "Update" 
            update_toggle.button_type = "primary"

    update_toggle = Toggle(label="Update", button_type="primary")
    update_toggle.on_change("active", update_toggle_callback)

    def device_select_callback(_attr, _old, new):
        # reset figures
        doc.add_next_tick_callback(_update_plots)
       # doc.add_next_tick_callback(_reset_calib_plot)

        # update default widget values

    
    def push_fit_elog_button_callback():
        device_name = device_select.value
        domain = get_device_domain(device_name)

        msg_id = push_elog(
            figures=((parameter_time_evolution_fig, "Symmetry_Parameter.png"),(symmetry_mean_fig, "Mean_Spectrum.png")),
            message="Symmetry",
            attributes={
                "Author": "sf-photodiag",
                "Entry": "Info",
                "Domain": domain,
                "System": "Diagnostics",
                "Title": f"{device_name} Symmetry results",
            },
        )
        log.info(
            f"Logbook entry created for {device_name}: "
            f"https://elog-gfa.psi.ch/SF-Photonics-Data/{msg_id}"
        )

    push_fit_elog_button = Button(label="Push plot elog")
    push_fit_elog_button.on_click(push_fit_elog_button_callback)

    def medianSpectrumIndex(spectrum):
        tot = 0
        for i in range(len(spectrum)):
            tot += spectrum[i]**2
        sumTo = (tot+1)/2
        add = 0
        j = 0
        while(sumTo > (add + spectrum[j]**2/2)):
            add += spectrum[j]**2
            j += 1
        return j
       

    async def _update_plots():

        if not buffer:
            mean_shot_line_source.data.update(x=[], y=[])
            single_shot_line_source.data.update(x=[], y=[])
            parameter_time_evolution_source.data.update(x=[],skew=[])
    
            return

        pv_x = pvs_x[device_select.value]
        pv_y = pvs_y[device_select.value]
        """
        single_shot_mean_index = round(scipy.ndimage.center_of_mass(pv_y.value)[0])
        single_shot_mean = pv_x.value[single_shot_mean_index]
        single_shot_median_index = medianSpectrumIndex(pv_y.value)
        single_shot_median = pv_x.value[single_shot_median_index]
        single_shot_median_line.location = single_shot_median
        single_shot_mean_line.location = single_shot_mean
        """

        sigmaArray = np.array(list(sigmaBuffer))
        #sigme_last = sigmaArray[-1]
        sigma_average = np.mean(sigmaArray)


        data_array = np.array(list(buffer))
        mean_spectrum = np.mean(data_array, axis=0)
        mean_shot_line_source.data.update(x=pv_x.value, y=mean_spectrum)
        average_mean_index = round(scipy.ndimage.center_of_mass(mean_spectrum)[0])
        average_mean = pv_x.value[average_mean_index]
        average_median_index = medianSpectrumIndex(mean_spectrum)
        average_median = pv_x.value[average_median_index]
        average_median_line.location = average_median
        average_mean_line.location = average_mean
    
        # update glyph sources
        single_shot_line_source.data.update(x=pv_x.value, y=pv_y.value)
        skew = 3*(average_mean-average_median)/sigma_average
        epics.caput('SAROP31-SPEC:skew',skew)
        
        parameter_time_evolution_source.stream(
            dict(
                x=[datetime.now()],
                skew = [skew],
            ),
            rollover=300,
        )

    device_select = Select(title="Device:", options=devices)
    device_select.on_change("value", device_select_callback)
    device_select.value = devices[0]

    num_shots_spinner = Spinner(title="Number shots:", mode="int", value=100, step=100, low=100)

    symmetry_layout = symmetry_fig
    symmetry_mean_layout = symmetry_mean_fig
    parameter_time_evolution_layout = parameter_time_evolution_fig
    fig_layout = column(symmetry_layout,symmetry_mean_layout, parameter_time_evolution_layout)
    tab_layout = column(
        fig_layout,
        row(
            device_select,
            num_shots_spinner,
            column(Spacer(height=18), update_toggle),
            column(Spacer(height=18), push_fit_elog_button),
        ),
    ) 


    return TabPanel(child=tab_layout, title=title)