from bokeh.layouts import column, row
from bokeh.models import Button, Div, TabPanel
from bokeh.plotting import curdoc
import epics as ep
import numpy as np
import time
from cam_server_client import CamClient

def create():
    # Create a new CamClient instance per session
    client = CamClient()
    
    # List of spectrometers based on provided data
    spectrometers = [
        "SARFE10-PSSS059",
        "SARFE10-PSSS059-LB",
        "SATOP21-PMOS127-1D",
        "SATOP21-PMOS127-2D",
        "SATOP31-PMOS132-1D",
        "SATOP31-PMOS132-2D",
        "SATOP11-PSAS079",
        "SATOP31-PSRD132",
        "SATOP31-PSRD175"
    ]
    
    # Create a new dictionary to store status messages for each spectrometer
    setup_status_texts = {spec: Div(text=f"Setup not run for {spec}", styles={"font-size": "14px"}) 
                          for spec in spectrometers}
    
    def configure_spectrometer(spectrometer):
        """ Read camera configuration, apply values to EPICS, update energy axis, and update status """
        try:
            # Retrieve camera configuration
            config = client.get_camera_config(spectrometer)
    
            if "hardware_configuration" not in config:
                raise RuntimeError(f"No hardware configuration found for {spectrometer}")
    
            # Store initial camera status
            initial_status = ep.caget(f"{spectrometer}:CAMERASTATUS")
    
            # Set camera to idle before updating configuration
            ep.caput(f"{spectrometer}:CAMERASTATUS", 1, wait=True)
            time.sleep(0.5)  # Small delay to ensure processing
    
            # Apply hardware configuration values to EPICS
            hardware_config = config["hardware_configuration"]
            for field, value in hardware_config.items():
                ep.caput(f"{spectrometer}:{field}", value, wait=True)
    
            # Apply settings using SET_PARAM
            ep.caput(f"{spectrometer}:SET_PARAM", 1, wait=True)
            time.sleep(1)
    
            # Update energy axis
            update_energy_axis(spectrometer)
    
            # Restore the initial camera status
            ep.caput(f"{spectrometer}:CAMERASTATUS", initial_status, wait=True)
    
            # Confirm update in UI
            setup_status_texts[spectrometer].text = f"<b>EPICS values and energy axis updated for:</b> {spectrometer}"
    
        except Exception as e:
            setup_status_texts[spectrometer].text = f"<b>Error:</b> {str(e)}"
    
    def update_energy_axis(spectrometer):
        """ Update the energy axis based on camera width or height """
        try:
            # Determine if WIDTH or HEIGHT should be used
            if "PSSS_LB" in spectrometer or "PSRD" in spectrometer:
                axis_length = ep.caget(f"{spectrometer}:HEIGHT")
            else:
                axis_length = ep.caget(f"{spectrometer}:WIDTH")
    
            if axis_length is None or axis_length <= 0:
                raise ValueError(f"Invalid axis length received for {spectrometer}")
    
            # Generate new energy axis
            new_axis = np.arange(0, axis_length)
            e_axis_name = f"{spectrometer}:SPECTRUM_X"
    
            # Write to EPICS PV
            result_caput = ep.caput(e_axis_name, new_axis.tolist(), wait=True)
    
            if result_caput is None or result_caput == 0:
                raise RuntimeError(f"Failed to update energy axis for {spectrometer}")
    
        except Exception as e:
            setup_status_texts[spectrometer].text += f" <b>Error updating energy axis:</b> {str(e)}"
    
    # Create a button for each spectrometer
    spectrometer_buttons = []
    for spec in spectrometers:
        button = Button(label=f"Setup {spec}", button_type="success")
        # Use lambda with default argument to capture current spectrometer
        button.on_click(lambda spectro=spec: configure_spectrometer(spectro))
        spectrometer_buttons.append(column(button, setup_status_texts[spec]))
    
    # Organize into two columns with appropriate colors
    aramis_buttons = [Div(text="<b>Aramis Spectrometers</b>", 
                           styles={"font-size": "16px", "background-color": "#9B1B30", "color": "white", "padding": "5px"})]
    athos_buttons = [Div(text="<b>Athos Spectrometers</b>", 
                          styles={"font-size": "16px", "background-color": "#FFC72C", "color": "black", "padding": "5px"})]
    
    for spec, button_col in zip(spectrometers, spectrometer_buttons):
        if spec.startswith("SAR"):
            aramis_buttons.append(button_col)
        else:
            athos_buttons.append(button_col)
    
    # Descriptive text for the panel
    description_text = Div(
        text="""
        <div style='background-color: #E8F4FF; padding: 10px; border-radius: 5px; font-size: 14px;'>
        <b>Spectrometer Setup – The “Fix It” Button! 🛠️✨</b><br>
        Pressing a Setup button will:<br>
        ✅ Reload the EPICS driver settings from the camera server config.<br>
        ✅ Update the energy axis length to match the current camera image size.<br>
        <br>
        <b>When to Press This Button?</b><br>
        🤔 Data not updating? <b>Press it</b>.<br>
        🎭 Numbers looking weird? <b>Press it</b>.<br>
        🔄 Just changed settings? <b>Press it</b>.<br>
        ❌ Bored? <b>Maybe… don’t</b>.<br>
        <br>
        <b>Press responsibly. 🚀🔧</b><br>
        <br>
        <a href='https://gitlab.psi.ch/photon-diagnostics/swissfel-photon-diagnostics-guide/-/wikis/Web-app-startup' target='_blank'><b>Full page guide</b></a>
        </div>
        """
    )
    
    # Build the layout
    layout = column(
        row(
            column(*aramis_buttons, width=300, styles={"background-color": "#9B1B30", "padding": "10px", "border-radius": "5px"}),
            column(*athos_buttons, width=300, styles={"background-color": "#FFC72C", "padding": "10px", "border-radius": "5px"}),
            sizing_mode="stretch_width"
        ),
        description_text
    )
    
    # Return a new TabPanel for this client
    return TabPanel(child=layout, title="Spectrometer Setup")