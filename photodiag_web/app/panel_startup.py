from collections import deque
from datetime import datetime
from threading import Lock, Thread
from time import sleep

import h5py as h5
import bsread
import numpy as np
import epics
import pandas as pd
import os
import logging
from bokeh.layouts import column, row
from bokeh.models import Div, TabPanel, Toggle, Spacer, TextAreaInput
from bokeh.plotting import curdoc

from cam_server_client import CamClient
from epics import caget, caput

def create():
    doc = curdoc()
    log = logging.getLogger(__name__)

    client = CamClient()
    lock = Lock()

    # Define camera groups
    camera_groups = {
        "Aramis": ["Beamline Aramis", "Alvra", "Bernina", "Cristallina"],
        "Athos": ["Beamline Athos", "Furka", "Maloja"]
    }

    camera_status_texts = {}

    # Load last update times from PSETUP configurations
    def get_last_update_time(psetup_name, field_key):
        config = client.get_camera_config(psetup_name)
        return config.get(field_key, "No record")

    aramis_last_update = get_last_update_time("SAROP10-PSETUP", "Aramis camera_calib_datetime")
    athos_last_update = get_last_update_time("SATOP10-PSETUP", "Athos camera_calib_datetime")

    # Logger output display
    log_textarea = TextAreaInput(title="Logging Output", height=150, width=800, disabled=True)

    def update_log(message):
        """ Update the logger text area in the UI. """
        doc.add_next_tick_callback(lambda: log_textarea.update(value=log_textarea.value + message + "\n"))

    def set_camera_config_list(list_name, button, psetup_name, field_key):
        cameras = client.get_camera_groups().get(list_name, [])
        log.info(f"Starting configuration for {list_name} cameras")
        update_log(f"Starting configuration for {list_name} cameras")

        for cam in cameras:
            config = client.get_camera_config(cam)
            if "hardware_configuration" in config.keys():
                set_camera_config(cam, config)

        # Update the last configuration time in PSETUP config
        config_datetime = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        psetup_config = client.get_camera_config(psetup_name)
        psetup_config[field_key] = config_datetime
        client.set_camera_config(psetup_name, psetup_config)

        # Update the text display in the UI
        doc.add_next_tick_callback(lambda: update_status_text(list_name, button, config_datetime))

    def set_camera_config(camera_name, config):
        try:
            initial_status = caget(camera_name + ':CAMERASTATUS')
            caput(camera_name + ':CAMERASTATUS', 1)
            sleep(0.5)  # Small delay to ensure processing

            for value in config['hardware_configuration']:
                pv_name = camera_name + ':' + value
                pv_value = config['hardware_configuration'][value]
                result = caput(pv_name, pv_value, wait=True)
                if result is None or result == 0:
                    log.warning(f"Failed to set {pv_name} to {pv_value}")
                    update_log(f"Failed to set {pv_name} to {pv_value}")
                else:
                    log.info(f"Set {pv_name} to {pv_value}")
                    update_log(f"Set {pv_name} to {pv_value}")

            caput(camera_name + ':SET_PARAM', 1)
            sleep(1)
            caput(camera_name + ':CAMERASTATUS', initial_status, wait=True)

        except Exception as e:
            log.error(f"Error setting camera config for {camera_name}: {e}")
            update_log(f"Error setting camera config for {camera_name}: {e}")

    def update_status_text(list_name, button, update_time):
        camera_status_texts[list_name].text = f"<b>Last push:</b> {update_time}"
        button.label = f"Push {list_name}"  # Reset button label after completion

    def create_camera_button(list_name, psetup_name, field_key, last_update_time):
        button = Toggle(label=f"Push {list_name}", button_type="primary", width=200)
        status_text = Div(text=f"Last push: {last_update_time}", styles={"font-size": "14px"})
        camera_status_texts[list_name] = status_text

        def callback(_attr, _old, new):
            if new:
                button.label = "Setting parameters..."
                thread = Thread(target=set_camera_config_list, args=(list_name, button, psetup_name, field_key))
                thread.start()

        button.on_change("active", callback)
        return column(button, status_text)

    # Create Aramis column
    aramis_buttons = [Div(text="<b>Aramis Cameras</b>", styles={"font-size": "16px", "background-color": "#9B1B30", "padding": "5px"})]
    for group in camera_groups["Aramis"]:
        aramis_buttons.append(create_camera_button(group, "SAROP10-PSETUP", "Aramis camera_calib_datetime", aramis_last_update))

    # Create Athos column
    athos_buttons = [Div(text="<b>Athos Cameras</b>", styles={"font-size": "16px", "background-color": "#FFC72C", "padding": "5px"})]
    for group in camera_groups["Athos"]:
        athos_buttons.append(create_camera_button(group, "SATOP10-PSETUP", "Athos camera_calib_datetime", athos_last_update))

    # Add the image
    image_div = Div(text="<img src='https://docs.google.com/drawings/d/1CWNBoGult1DAjGu2zTZrFK_vU0Pe5RVMEMkuNv_URXY/pub?w=960&h=720' width='900'>")

    # Layout with two shaded columns and an image
    layout = column(
        row(
            column(*aramis_buttons, width=300, styles={"background-color": "#9B1B30", "padding": "10px", "border-radius": "5px"}),
            column(*athos_buttons, width=300, styles={"background-color": "#FFC72C", "padding": "10px", "border-radius": "5px"}),
            sizing_mode="stretch_width"
        ),
        image_div,
        log_textarea
    )

    return TabPanel(child=layout, title="Camera Configurations")