from datahub import Daqbuf, Table
from datetime import datetime, timedelta
from threading import Lock, Thread, RLock
import numpy as np
import pandas as pd
from bokeh.layouts import column, row
from bokeh.models import Band, ColumnDataSource, Spacer, TabPanel, Toggle, Button, DatetimeTickFormatter
from bokeh.plotting import curdoc, figure

class DataFetcher:
    """Class to fetch data from the SwissFEL databuffer."""
    def __init__(self, lock, log):
        self.lock = lock
        self.log = log
        self.data = {}

    def fetch_data(self, channels, from_time, to_time):
        """Fetch data for specified channels and time range."""
        try:
            print("[INFO] Starting data fetch...")
            query = {
                "channels": channels,
                "start": from_time,
                "end": to_time
            }
            
            with Daqbuf(backend="sf-databuffer", cbor=True) as source:
                table = Table()
                source.add_listener(table)
                source.request(query)
                dataframe = table.as_dataframe(index=Table.TIMESTAMP)
                
                # Debug print for dataframe
                print("[DEBUG] Dataframe head:")
                print(dataframe.head())

                values = {"timestamp": dataframe.index.to_numpy()}  # Add timestamps to values
                for channel in channels:
                    if channel in dataframe.columns:
                        values[channel] = dataframe[channel].to_numpy()
                        print(f"[DEBUG] Channel {channel}: {len(values[channel])} data points fetched.")
                    else:
                        print(f"[ERROR] Channel {channel} not found in the dataframe.")
                
                # Thread-safe data assignment
                with self.lock:
                    self.data = values
                
                print(f"[INFO] Data fetch complete. Channels retrieved: {list(self.data.keys())}")

        except Exception as e:
            print(f"[ERROR] Error fetching or processing data: {e}")
            with self.lock:
                self.data = {}

def initialize_plot(title, y_axis_label):
    """Create a generic Bokeh plot."""
    plot = figure(
        height=250,
        width=1000,
        x_axis_label="Time",
        x_axis_type="datetime",
        y_axis_label=y_axis_label,
        title=title,
        tools="pan,wheel_zoom,save,reset",
    )
    plot.toolbar.logo = None
    plot.xaxis.formatter = DatetimeTickFormatter(
        hours=["%Y-%m-%d %H:%M"],
        days=["%Y-%m-%d"],
        months=["%Y-%m"],
        years=["%Y"]
    )
    return plot

def update_plot(fig, data, lock, channel, y_label):
    """Update the plot with data for a specific channel."""
    print(f"[DEBUG] Attempting to update plot for channel {channel}.")
    with lock:
        if not data:
            print("[DEBUG] No data available to update the plot.")
            return

    if channel not in data:
        print(f"[ERROR] Channel {channel} not found in data for plotting.")
        return

    channel_data = data[channel]
    timestamps = pd.to_datetime(data["timestamp"])
    print(f"[INFO] Updating plot for channel {channel} with {len(channel_data)} data points.")

    try:
        # Create DataFrame for plotting
        df = pd.DataFrame(data=dict(x=timestamps, y=channel_data))
        rolling_window = 50 if len(channel_data) > 1000 else 300
        df2 = df.y.rolling(window=rolling_window).agg({"y_mean": "mean", "y_std": "std"})
        df = pd.concat([df, df2], axis=1)
        df["lower"] = df.y_mean - df.y_std
        df["upper"] = df.y_mean + df.y_std

        # Update the plot
        source = ColumnDataSource(df.reset_index())
        fig.renderers = []
        fig.line("x", "y_mean", line_width=2, source=source, legend_label=f"{channel} {y_label}")
        band = Band(base="x", lower="lower", upper="upper", source=source,
                    fill_alpha=0.3, fill_color="yellow", line_color="black")
        fig.add_layout(band)
        print(f"[INFO] Plot for channel {channel} updated successfully.")

    except Exception as e:
        print(f"[ERROR] Failed to update plot for channel {channel}: {e}")

def create():
    """Create the Bokeh dashboard."""
    doc = curdoc()
    log = doc.logger
    lock = RLock()
    data_fetcher = DataFetcher(lock, log)

    # Initialize the plots
    summary_fig = initialize_plot("Intensity Plot", "Intensity")
    xpos_fig = initialize_plot("XPOS Plot", "X Position")
    ypos_fig = initialize_plot("YPOS Plot", "Y Position")

    def fetch_data(channels, from_time, to_time):
        thread = Thread(target=data_fetcher.fetch_data, args=(channels, from_time, to_time))
        thread.start()

        def check_data_ready():
            with lock:
                if data_fetcher.data:
                    print("[INFO] Data ready for plotting.")
                    update_plot(summary_fig, data_fetcher.data, lock, "SAROP21-PBPS133:INTENSITY", "Intensity")
                    update_plot(xpos_fig, data_fetcher.data, lock, "SAROP21-PBPS133:XPOS", "X Position")
                    update_plot(ypos_fig, data_fetcher.data, lock, "SAROP21-PBPS133:YPOS", "Y Position")
                else:
                    doc.add_timeout_callback(check_data_ready, 100)

        doc.add_timeout_callback(check_data_ready, 100)

    def fetch_last_hour(attr, old, new):
        if new:
            snap_1h.label = "Fetching data..."
            now = datetime.now()
            from_time, to_time = [
                (now - timedelta(minutes=m)).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
                for m in [61, 1]
            ]
            channels = [
                "SAROP21-PBPS133:INTENSITY",
                "SAROP21-PBPS133:XPOS",
                "SAROP21-PBPS133:YPOS"
            ]
            fetch_data(channels, from_time, to_time)
            snap_1h.label = "1 hour snapshot"

    def fetch_last_day():
        now = datetime.now()
        from_time, to_time = [
            (now - timedelta(days=1)).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],
            now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        ]
        channels = [
            "SAROP21-PBPS133:INTENSITY",
            "SAROP21-PBPS133:XPOS",
            "SAROP21-PBPS133:YPOS"
        ]
        fetch_data(channels, from_time, to_time)

    def fetch_last_week():
        now = datetime.now()
        from_time, to_time = [
            (now - timedelta(weeks=1)).strftime('%Y-%m-%d %H:%M:%S.%f')[:-3],
            now.strftime('%Y-%m-%d %H:%M:%S.%f')[:-3]
        ]
        channels = [
            "SAROP21-PBPS133:INTENSITY",
            "SAROP21-PBPS133:XPOS",
            "SAROP21-PBPS133:YPOS"
        ]
        fetch_data(channels, from_time, to_time)

    snap_1h = Toggle(label="1 hour snapshot", button_type="primary")
    snap_1h.on_change("active", fetch_last_hour)

    day_button = Button(label="1 day snapshot", button_type="primary")
    day_button.on_click(fetch_last_day)

    week_button = Button(label="1 week snapshot", button_type="primary")
    week_button.on_click(fetch_last_week)

    fig_layout = column(summary_fig, xpos_fig, ypos_fig)
    tab_layout = column(
        fig_layout,
        row(column(Spacer(height=18), snap_1h, day_button, week_button)),
    )

    return TabPanel(child=tab_layout, title="Summary and Position Plots")
